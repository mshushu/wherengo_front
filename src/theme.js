import {
    cyan500, cyan700,
    pinkA200, pinkA400,
    grey100, grey900,
    grey300, grey400, grey500,
    white, darkBlack, fullBlack
} from 'material-ui/styles/colors';

import {fade} from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';

export default {
    palette: {
        primary1Color: cyan500,
        accent1Color: pinkA400,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        disabledColor: fade(darkBlack, 0.3),
        pickerHeaderColor: cyan500,
        clockCircleColor: fade(darkBlack, 0.07),
        shadowColor: fullBlack
    },
}
