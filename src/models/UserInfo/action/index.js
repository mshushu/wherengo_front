import CONFIG from '../../../utility/function/config';

export const USER_INFO = 'USER_INFO';
export const USER_INFO_REQUEST = 'USER_INFO_REQUEST';
export const USER_INFO_SUCCESS = 'USER_INFO_SUCCESS';
export const USER_INFO_FAILURE = 'USER_INFO_FAILURE';

export const requestUserInfo = (token) => {
  if(!token) {
    return {
      type: USER_INFO_FAILURE,
      data: {
        success: false,
        message: 'token is empty'
      }
    }
  }
  return {
      type: USER_INFO,
      promise: { method: 'post', url: `${CONFIG.host}/api/user_info`, data: { token } }
  };
};
