import {USER_INFO_REQUEST, USER_INFO_SUCCESS, USER_INFO_FAILURE } from '../action';

const defaultState = {
    result: null,
    pendding: false
};

const UserInfo = (state = defaultState, action) => {
    switch (action.type) {
        case USER_INFO_REQUEST:
            return {
                ...state,
                pendding: true,
                result: null,
            };
        case USER_INFO_SUCCESS:
            return {
                ...state,
                pendding: false,
                result: action.data
            };
        case USER_INFO_FAILURE:
            return {
                ...state,
                pendding: false,
                result: action.data
            };
        default:
          return state
    }
}

export default UserInfo;
