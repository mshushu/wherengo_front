import CONFIG from '../../../utility/function/config';

export const GET_JAPAN_AREA = 'GET_JAPAN_AREA';
export const GET_JAPAN_AREA_REQUEST = 'GET_JAPAN_AREA_REQUEST';
export const GET_JAPAN_AREA_SUCCESS = 'GET_JAPAN_AREA_SUCCESS';
export const GET_JAPAN_AREA_FAILURE = 'GET_JAPAN_AREA_FAILURE';

export const getJapanArea = () => {
  return {
    type: GET_JAPAN_AREA,
    promise: {
      method: 'GET',
      url: `${CONFIG.host}/api/japan_areas`,
    },
  };
};
