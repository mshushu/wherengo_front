import {
  GET_JAPAN_AREA_REQUEST,
  GET_JAPAN_AREA_SUCCESS,
  GET_JAPAN_AREA_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const JapanArea = (state = defaultState, action) => {
  switch (action.type) {
    case GET_JAPAN_AREA_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case GET_JAPAN_AREA_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case GET_JAPAN_AREA_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default JapanArea;
