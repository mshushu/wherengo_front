import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import reducer from './index';
import promiseMiddleware from './middleware/promiseMiddleware';

export default function(initialState) {
  const enhancer = composeWithDevTools(applyMiddleware(promiseMiddleware));
  return createStore(reducer, initialState, enhancer);
}
