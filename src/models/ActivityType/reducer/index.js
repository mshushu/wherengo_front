import {
  GET_ACTIVITY_TYPE_REQUEST,
  GET_ACTIVITY_TYPE_SUCCESS,
  GET_ACTIVITY_TYPE_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const ActivityType = (state = defaultState, action) => {
  switch (action.type) {
    case GET_ACTIVITY_TYPE_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case GET_ACTIVITY_TYPE_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case GET_ACTIVITY_TYPE_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default ActivityType;
