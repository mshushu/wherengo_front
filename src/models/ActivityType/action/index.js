import CONFIG from '../../../utility/function/config';

export const GET_ACTIVITY_TYPE = 'GET_ACTIVITY_TYPE';
export const GET_ACTIVITY_TYPE_REQUEST = 'GET_ACTIVITY_TYPE_REQUEST';
export const GET_ACTIVITY_TYPE_SUCCESS = 'GET_ACTIVITY_TYPE_SUCCESS';
export const GET_ACTIVITY_TYPE_FAILURE = 'GET_ACTIVITY_TYPE_FAILURE';

export const getActivityType = () => {
  return {
    type: GET_ACTIVITY_TYPE,
    promise: {
      method: 'GET',
      url: `${CONFIG.host}/api/activity_types`,
    },
  };
};
