import CONFIG from '../../../utility/function/config';

export const GET_DONATION_TYPE = 'GET_DONATION_TYPE';
export const GET_DONATION_TYPE_REQUEST = 'GET_DONATION_TYPE_REQUEST';
export const GET_DONATION_TYPE_SUCCESS = 'GET_DONATION_TYPE_SUCCESS';
export const GET_DONATION_TYPE_FAILURE = 'GET_DONATION_TYPE_FAILURE';

const URL = `${CONFIG.host}/api/donation_types`;
const ADMIN_URL = `${CONFIG.host}/api/admin/donation_types`;

export const getDonationType = () => {
  return {
    type: GET_DONATION_TYPE,
    promise: {
      method: 'GET',
      url: URL,
    },
  };
};
