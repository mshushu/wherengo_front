import {
  GET_DONATION_TYPE,
  GET_DONATION_TYPE_SUCCESS,
  GET_DONATION_TYPE_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const DonationType = (state = defaultState, action) => {
  switch (action.type) {
    case GET_DONATION_TYPE:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case GET_DONATION_TYPE_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case GET_DONATION_TYPE_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default DonationType;
