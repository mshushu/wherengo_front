import CONFIG from '../../../utility/function/config';

export const GET_WORLD_AREA = 'GET_WORLD_AREA';
export const GET_WORLD_AREA_REQUEST = 'GET_WORLD_AREA_REQUEST';
export const GET_WORLD_AREA_SUCCESS = 'GET_WORLD_AREA_SUCCESS';
export const GET_WORLD_AREA_FAILURE = 'GET_WORLD_AREA_FAILURE';

export const getWorldArea = () => {
  return {
    type: GET_WORLD_AREA,
    promise: {
      method: 'GET',
      url: `${CONFIG.host}/api/world_areas`,
    },
  };
};
