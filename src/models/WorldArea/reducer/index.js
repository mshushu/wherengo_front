import {
  GET_WORLD_AREA_REQUEST,
  GET_WORLD_AREA_SUCCESS,
  GET_WORLD_AREA_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const WorldArea = (state = defaultState, action) => {
  switch (action.type) {
    case GET_WORLD_AREA_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case GET_WORLD_AREA_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case GET_WORLD_AREA_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default WorldArea;
