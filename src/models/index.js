import { combineReducers } from 'redux';
import ActivityType from './ActivityType/reducer';
import ActivityDetailType from './ActivityDetailType/reducer';
import JapanArea from './JapanArea/reducer';
import WorldArea from './WorldArea/reducer';
import Organizations from './Organizations/reducer';
import HomeInfomations from './HomeInfomations/reducer';

const reducer = combineReducers({
  ActivityType,
  ActivityDetailType,
  JapanArea,
  WorldArea,
  Organizations,
  HomeInfomations,
});

export default reducer;
