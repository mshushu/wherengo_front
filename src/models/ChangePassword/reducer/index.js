import {
  CHANGE_PASSWORD_KEY_REQUEST,
  CHANGE_PASSWORD_KEY_SUCCESS,
  CHANGE_PASSWORD_KEY_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const ChangePassword = (state = defaultState, action) => {
  switch (action.type) {
    case CHANGE_PASSWORD_KEY_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case CHANGE_PASSWORD_KEY_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case CHANGE_PASSWORD_KEY_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default ChangePassword;
