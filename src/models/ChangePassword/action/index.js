import CONFIG from '../../../utility/function/config';

export const CHANGE_PASSWORD_KEY = 'CHANGE_PASSWORD_KEY';
export const CHANGE_PASSWORD_KEY_REQUEST = 'CHANGE_PASSWORD_KEY_REQUEST';
export const CHANGE_PASSWORD_KEY_SUCCESS = 'CHANGE_PASSWORD_KEY_SUCCESS';
export const CHANGE_PASSWORD_KEY_FAILURE = 'CHANGE_PASSWORD_KEY_FAILURE';

export const requestChangePasswordKey = (url, password, confirm, key) => {
  return {
    type: CHANGE_PASSWORD_KEY,
    promise: {
      method: 'POST',
      url: `${CONFIG.host}/api/admin/change_password_key`,
      data: {
        url,
        password,
        confirm,
        key,
      },
    },
  };
};
