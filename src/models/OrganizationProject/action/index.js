import CONFIG from '../../../utility/function/config';

export const GET_ORGANIZATION_PROJECT = 'GET_ORGANIZATION_PROJECT';
export const GET_ORGANIZATION_PROJECT_REQUEST =
  'GET_ORGANIZATION_PROJECT_REQUEST';
export const GET_ORGANIZATION_PROJECT_SUCCESS =
  'GET_ORGANIZATION_PROJECT_SUCCESS';
export const GET_ORGANIZATION_PROJECT_FAILURE =
  'GET_ORGANIZATION_PROJECT_FAILURE';

export const SET_ORGANIZATION_PROJECT = 'SET_ORGANIZATION_PROJECT';
export const SET_ORGANIZATION_PROJECT_REQUEST =
  'SET_ORGANIZATION_PROJECT_REQUEST';
export const SET_ORGANIZATION_PROJECT_SUCCESS =
  'SET_ORGANIZATION_PROJECT_SUCCESS';
export const SET_ORGANIZATION_PROJECT_FAILURE =
  'SET_ORGANIZATION_PROJECT_FAILURE';

export const UPDATE_ORGANIZATION_PROJECT = 'UPDATE_ORGANIZATION_PROJECT';
export const UPDATE_ORGANIZATION_PROJECT_REQUEST =
  'UPDATE_ORGANIZATION_PROJECT_REQUEST';
export const UPDATE_ORGANIZATION_PROJECT_SUCCESS =
  'UPDATE_ORGANIZATION_PROJECT_SUCCESS';
export const UPDATE_ORGANIZATION_PROJECT_FAILURE =
  'UPDATE_ORGANIZATION_PROJECT_FAILURE';

export const RESET_ORGANIZATION_PROJECT_REQUEST = 'RESET_ORGANIZATION_PROJECT_REQUEST';

const URL = `${CONFIG.host}/api/organization_projects`;
const ADMIN_URL = `${CONFIG.host}/api/admin/organization_projects`;

export const getOrganizationProject = (id) => {
  return {
    type: GET_ORGANIZATION_PROJECT,
    promise: {
      method: 'GET',
      url: URL + `/${id}`,
    },
  };
};

export const setOrganizationProject = (token, data) => {
  if (!token) {
    return {
      type: SET_ORGANIZATION_PROJECT_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: SET_ORGANIZATION_PROJECT,
    promise: {
      method: 'POST',
      url: ADMIN_URL,
      token: token,
      data: data,
    },
  };
};

export const updateOrganizationProject = (token, data) => {
  if (!token) {
    return {
      type: UPDATE_ORGANIZATION_PROJECT_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  data.append('_method', 'PUT');
  return {
    type: UPDATE_ORGANIZATION_PROJECT,
    promise: {
      method: 'POST',
      url: ADMIN_URL + `/${data.get('organization_seq')}`,
      token: token,
      data: data,
    },
  };
};

export const resetOrganizationProject = () => {
  return {
    type: RESET_ORGANIZATION_PROJECT_REQUEST,
  };
};