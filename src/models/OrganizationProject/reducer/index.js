import {
  GET_ORGANIZATION_PROJECT_REQUEST,
  GET_ORGANIZATION_PROJECT_SUCCESS,
  GET_ORGANIZATION_PROJECT_FAILURE,
  SET_ORGANIZATION_PROJECT_REQUEST,
  SET_ORGANIZATION_PROJECT_SUCCESS,
  SET_ORGANIZATION_PROJECT_FAILURE,
  UPDATE_ORGANIZATION_PROJECT_REQUEST,
  UPDATE_ORGANIZATION_PROJECT_SUCCESS,
  UPDATE_ORGANIZATION_PROJECT_FAILURE,
  RESET_ORGANIZATION_PROJECT_REQUEST
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const OrganizationProject = (state = defaultState, action) => {
  switch (action.type) {
    case GET_ORGANIZATION_PROJECT_REQUEST:
    case SET_ORGANIZATION_PROJECT_REQUEST:
    case UPDATE_ORGANIZATION_PROJECT_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case GET_ORGANIZATION_PROJECT_SUCCESS:
    case SET_ORGANIZATION_PROJECT_SUCCESS:
    case UPDATE_ORGANIZATION_PROJECT_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case GET_ORGANIZATION_PROJECT_FAILURE:
    case SET_ORGANIZATION_PROJECT_FAILURE:
    case UPDATE_ORGANIZATION_PROJECT_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case RESET_ORGANIZATION_PROJECT_REQUEST:
      return {
        ...state,
        pendding: false,
        result: null,
      };
    default:
      return state;
  }
};

export default OrganizationProject;
