import CONFIG from '../../../utility/function/config';

export const GET_ORGANIZATION_KIND = 'GET_ORGANIZATION_KIND';
export const GET_ORGANIZATION_KIND_REQUEST = 'GET_ORGANIZATION_KIND_REQUEST';
export const GET_ORGANIZATION_KIND_SUCCESS = 'GET_ORGANIZATION_KIND_SUCCESS';
export const GET_ORGANIZATION_KIND_FAILURE = 'GET_ORGANIZATION_KIND_FAILURE';

export const PUBLISH_ORGANIZATION_KIND = 'PUBLISH_ORGANIZATION_KIND';
export const PUBLISH_ORGANIZATION_KIND_REQUEST = 'PUBLISH_ORGANIZATION_KIND_REQUEST';
export const PUBLISH_ORGANIZATION_KIND_SUCCESS = 'PUBLISH_ORGANIZATION_KIND_SUCCESS';
export const PUBLISH_ORGANIZATION_KIND_FAILURE = 'PUBLISH_ORGANIZATION_KIND_FAILURE';

export const DELETE_ORGANIZATION_KIND = 'DELETE_ORGANIZATION_KIND';
export const DELETE_ORGANIZATION_KIND_REQUEST = 'DELETE_ORGANIZATION_KIND_REQUEST';
export const DELETE_ORGANIZATION_KIND_SUCCESS = 'DELETE_ORGANIZATION_KIND_SUCCESS';
export const DELETE_ORGANIZATION_KIND_FAILURE = 'DELETE_ORGANIZATION_KIND_FAILURE';

export const SET_ORGANIZATION_KIND = 'SET_ORGANIZATION_KIND';
export const SET_ORGANIZATION_KIND_REQUEST = 'SET_ORGANIZATION_KIND_REQUEST';
export const SET_ORGANIZATION_KIND_SUCCESS = 'SET_ORGANIZATION_KIND_SUCCESS';
export const SET_ORGANIZATION_KIND_FAILURE = 'SET_ORGANIZATION_KIND_FAILURE';

export const UPDATE_ORGANIZATION_KIND = 'UPDATE_ORGANIZATION_KIND';
export const UPDATE_ORGANIZATION_KIND_REQUEST = 'UPDATE_ORGANIZATION_KIND_REQUEST';
export const UPDATE_ORGANIZATION_KIND_SUCCESS = 'UPDATE_ORGANIZATION_KIND_SUCCESS';
export const UPDATE_ORGANIZATION_KIND_FAILURE = 'UPDATE_ORGANIZATION_KIND_FAILURE';

const URL = `${CONFIG.host}/api/organization_kinds`;
const ADMIN_URL = `${CONFIG.host}/api/admin/organization_kinds`;

export const getOrganizationKind = () => {
  return {
    type: GET_ORGANIZATION_KIND,
    promise: {
      method: 'GET',
      url: URL,
    },
  };
};

export const publishOrganizationKind = (token, seq, publish) => {
  if (!token) {
    return {
      type: PUBLISH_ORGANIZATION_KIND_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: PUBLISH_ORGANIZATION_KIND,
    promise: {
      method: 'PUT',
      url: ADMIN_URL + `/${seq}`,
      token: token,
      data: {
        type: 'PUBLISH_ORGANIZATION_KIND',
        publish,
      },
    },
  };
};

export const deleteOrganizationKind = (token, seq) => {
  if (!token) {
    return {
      type: DELETE_ORGANIZATION_KIND_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: DELETE_ORGANIZATION_KIND,
    promise: {
      method: 'DELETE',
      url: ADMIN_URL + `/${seq}`,
      token: token,
    },
  };
};

export const setOrganizationKind = (token, data) => {
  if (!token) {
    return {
      type: SET_ORGANIZATION_KIND_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: SET_ORGANIZATION_KIND,
    promise: {
      method: 'POST',
      url: ADMIN_URL,
      token: token,
      data: data,
    },
  };
};

export const updateOrganizationKind = (token, data) => {
  if (!token) {
    return {
      type: UPDATE_ORGANIZATION_KIND_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  data.append('_method', 'PUT');
  return {
    type: UPDATE_ORGANIZATION_KIND,
    promise: {
      method: 'POST',
      url: ADMIN_URL + `/${data.get('seq')}`,
      token: token,
      data: data,
    },
  };
};
