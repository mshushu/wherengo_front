import {
  GET_ORGANIZATION_KIND_REQUEST,
  GET_ORGANIZATION_KIND_SUCCESS,
  GET_ORGANIZATION_KIND_FAILURE,
  PUBLISH_ORGANIZATION_KIND_REQUEST,
  PUBLISH_ORGANIZATION_KIND_SUCCESS,
  PUBLISH_ORGANIZATION_KIND_FAILURE,
  DELETE_ORGANIZATION_KIND_REQUEST,
  DELETE_ORGANIZATION_KIND_SUCCESS,
  DELETE_ORGANIZATION_KIND_FAILURE,
  SET_ORGANIZATION_KIND_REQUEST,
  SET_ORGANIZATION_KIND_SUCCESS,
  SET_ORGANIZATION_KIND_FAILURE,
  UPDATE_ORGANIZATION_KIND_REQUEST,
  UPDATE_ORGANIZATION_KIND_SUCCESS,
  UPDATE_ORGANIZATION_KIND_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const OrganizationKind = (state = defaultState, action) => {
  switch (action.type) {
    case GET_ORGANIZATION_KIND_REQUEST:
    case SET_ORGANIZATION_KIND_REQUEST:
    case UPDATE_ORGANIZATION_KIND_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case GET_ORGANIZATION_KIND_SUCCESS:
    case SET_ORGANIZATION_KIND_SUCCESS:
    case UPDATE_ORGANIZATION_KIND_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case GET_ORGANIZATION_KIND_FAILURE:
    case SET_ORGANIZATION_KIND_FAILURE:
    case UPDATE_ORGANIZATION_KIND_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case PUBLISH_ORGANIZATION_KIND_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case PUBLISH_ORGANIZATION_KIND_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case PUBLISH_ORGANIZATION_KIND_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };

    case DELETE_ORGANIZATION_KIND_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case DELETE_ORGANIZATION_KIND_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case DELETE_ORGANIZATION_KIND_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default OrganizationKind;
