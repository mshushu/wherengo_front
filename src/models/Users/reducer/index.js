import {
  USERS_REQUEST,
  USERS_SUCCESS,
  USERS_FAILURE,
  USERS_DELETE_REQUEST,
  USERS_DELETE_SUCCESS,
  USERS_DELETE_FAILURE,
  MASTER_USERS_CREATE_REQUEST,
  MASTER_USERS_CREATE_SUCCESS,
  MASTER_USERS_CREATE_FAILURE,
  INVITE_USERS_REQUEST,
  INVITE_USERS_SUCCESS,
  INVITE_USERS_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const UserInfo = (state = defaultState, action) => {
  switch (action.type) {
    case USERS_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case USERS_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case USERS_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };

    case USERS_DELETE_REQUEST:
    case MASTER_USERS_CREATE_REQUEST:
    case INVITE_USERS_REQUEST:
      return {
        ...state,
        pendding: true,
        result: action.data,
      };
    case USERS_DELETE_SUCCESS:
    case MASTER_USERS_CREATE_SUCCESS:
    case INVITE_USERS_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case USERS_DELETE_FAILURE:
    case MASTER_USERS_CREATE_FAILURE:
    case INVITE_USERS_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default UserInfo;
