import CONFIG from '../../../utility/function/config';

export const USERS = 'USERS';
export const USERS_REQUEST = 'USERS_REQUEST';
export const USERS_SUCCESS = 'USERS_SUCCESS';
export const USERS_FAILURE = 'USERS_FAILURE';

export const USERS_DELETE = 'USERS_DELETE';
export const USERS_DELETE_REQUEST = 'USERS_DELETE_REQUEST';
export const USERS_DELETE_SUCCESS = 'USERS_DELETE_SUCCESS';
export const USERS_DELETE_FAILURE = 'USERS_DELETE_FAILURE';

export const MASTER_USERS_CREATE = 'MASTER_USERS_CREATE';
export const MASTER_USERS_CREATE_REQUEST = 'MASTER_USERS_CREATE_REQUEST';
export const MASTER_USERS_CREATE_SUCCESS = 'MASTER_USERS_CREATE_SUCCESS';
export const MASTER_USERS_CREATE_FAILURE = 'MASTER_USERS_CREATE_FAILURE';

export const INVITE_USERS = 'INVITE_USERS';
export const INVITE_USERS_REQUEST = 'INVITE_USERS_REQUEST';
export const INVITE_USERS_SUCCESS = 'INVITE_USERS_SUCCESS';
export const INVITE_USERS_FAILURE = 'INVITE_USERS_FAILURE';

const URL = `${CONFIG.host}/api/admin/users`;

export const requestUsers = token => {
  if (!token) {
    return {
      type: USERS_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: USERS,
    promise: {
      method: 'get',
      url: URL,
      token: token,
    },
  };
};

export const requestUserDelete = (token, seq) => {
  if (!token) {
    return {
      type: USERS_DELETE_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: USERS_DELETE,
    promise: {
      method: 'DELETE',
      url: URL + `/${seq}`,
      token: token,
    },
  };
};

export const requestMasterUserCreate = (token, email) => {
  if (!token) {
    return {
      type: MASTER_USERS_CREATE_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: MASTER_USERS_CREATE,
    promise: {
      method: 'POST',
      url: URL,
      token: token,
      data: {
        email: email,
      },
    },
  };
};

export const requestInviteUser = (token, email) => {
  if (!token) {
    return {
      type: INVITE_USERS_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: INVITE_USERS,
    promise: {
      method: 'POST',
      url: URL + '/invite',
      token: token,
      data: {
        email: email,
      },
    },
  };
};
