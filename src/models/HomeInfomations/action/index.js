import CONFIG from '../../../utility/function/config';

export const GET_HOME_ORGANIZATIONS = 'GET_HOME_ORGANIZATIONS';
export const GET_HOME_ORGANIZATIONS_REQUEST = 'GET_HOME_ORGANIZATIONS_REQUEST';
export const GET_HOME_ORGANIZATIONS_SUCCESS = 'GET_HOME_ORGANIZATIONS_SUCCESS';
export const GET_HOME_ORGANIZATIONS_FAILURE = 'GET_HOME_ORGANIZATIONS_FAILURE';

const URL = `${CONFIG.host}/api/search/top`;

export const getHomeOrganizations = params => {
  return {
    type: GET_HOME_ORGANIZATIONS,
    promise: {
      method: 'GET',
      url: URL,
    },
  };
};
