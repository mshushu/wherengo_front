import {
  GET_HOME_ORGANIZATIONS_REQUEST,
  GET_HOME_ORGANIZATIONS_SUCCESS,
  GET_HOME_ORGANIZATIONS_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const Organizations = (state = defaultState, action) => {
  switch (action.type) {
    case GET_HOME_ORGANIZATIONS_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case GET_HOME_ORGANIZATIONS_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case GET_HOME_ORGANIZATIONS_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default Organizations;
