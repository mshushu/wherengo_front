import axios from 'axios';

export default () => {
  return next => action => {
    const { promise, type, ...rest } = action;
    if (promise != null) {
      next({ ...rest, type: `${type}_REQUEST` });

      let headers = {
        authorization: 'Bearer ' + promise.token,
      };

      const csrf = document.querySelector('meta[name="csrf-token"]');
      if (csrf && csrf.getAttribute('content')) {
        headers['X-CSRF-TOKEN'] = csrf.getAttribute('content');
      }

      if (promise['Content-Type']) {
        headers['Content-Type'] = promise['Content-Type'];
      }

      return axios({
        method: promise.method,
        url: promise.url,
        data: promise.data,
        headers: headers,
      })
        .then(result => {
          next({ ...rest, data: result, type: `${type}_SUCCESS` });
        })
        .catch(error => {
          next({ ...rest, data: error, type: `${type}_FAILURE` });
        });
    } else {
      return next(action);
    }
  };
};
