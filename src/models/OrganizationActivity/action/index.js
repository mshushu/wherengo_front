import CONFIG from '../../../utility/function/config';

export const GET_ORGANIZATION_ACTIVITY = 'GET_ORGANIZATION_ACTIVITY';
export const GET_ORGANIZATION_ACTIVITY_REQUEST =
  'GET_ORGANIZATION_ACTIVITY_REQUEST';
export const GET_ORGANIZATION_ACTIVITY_SUCCESS =
  'GET_ORGANIZATION_ACTIVITY_SUCCESS';
export const GET_ORGANIZATION_ACTIVITY_FAILURE =
  'GET_ORGANIZATION_ACTIVITY_FAILURE';

export const SET_ORGANIZATION_ACTIVITY = 'SET_ORGANIZATION_ACTIVITY';
export const SET_ORGANIZATION_ACTIVITY_REQUEST =
  'SET_ORGANIZATION_ACTIVITY_REQUEST';
export const SET_ORGANIZATION_ACTIVITY_SUCCESS =
  'SET_ORGANIZATION_ACTIVITY_SUCCESS';
export const SET_ORGANIZATION_ACTIVITY_FAILURE =
  'SET_ORGANIZATION_ACTIVITY_FAILURE';

export const UPDATE_ORGANIZATION_ACTIVITY = 'UPDATE_ORGANIZATION_ACTIVITY';
export const UPDATE_ORGANIZATION_ACTIVITY_REQUEST =
  'UPDATE_ORGANIZATION_ACTIVITY_REQUEST';
export const UPDATE_ORGANIZATION_ACTIVITY_SUCCESS =
  'UPDATE_ORGANIZATION_ACTIVITY_SUCCESS';
export const UPDATE_ORGANIZATION_ACTIVITY_FAILURE =
  'UPDATE_ORGANIZATION_ACTIVITY_FAILURE';

export const RESET_ORGANIZATION_ACTIVITY_REQUEST = 'RESET_ORGANIZATION_ACTIVITY_REQUEST';

const URL = `${CONFIG.host}/api/organization_activities`;
const ADMIN_URL = `${CONFIG.host}/api/admin/organization_activities`;

export const getOrganizationActivity = (id) => {
  return {
    type: GET_ORGANIZATION_ACTIVITY,
    promise: {
      method: 'GET',
      url: URL + `/${id}`,
    },
  };
};

export const setOrganizationActivity = (token, data) => {
  if (!token) {
    return {
      type: SET_ORGANIZATION_ACTIVITY_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  return {
    type: SET_ORGANIZATION_ACTIVITY,
    promise: {
      method: 'POST',
      url: ADMIN_URL,
      token: token,
      data: data,
    },
  };
};

export const updateOrganizationActivity = (token, data) => {
  if (!token) {
    return {
      type: UPDATE_ORGANIZATION_ACTIVITY_FAILURE,
      data: {
        success: false,
        message: 'token is empty',
      },
    };
  }

  data.append('_method', 'PUT');
  return {
    type: UPDATE_ORGANIZATION_ACTIVITY,
    promise: {
      method: 'POST',
      url: ADMIN_URL + `/${data.get('organization_seq')}`,
      token: token,
      data: data,
    },
  };
};

export const resetOrganizationActivity = () => {
  return {
    type: RESET_ORGANIZATION_ACTIVITY_REQUEST,
  };
};