import {
  GET_ORGANIZATION_ACTIVITY_REQUEST,
  GET_ORGANIZATION_ACTIVITY_SUCCESS,
  GET_ORGANIZATION_ACTIVITY_FAILURE,
  SET_ORGANIZATION_ACTIVITY_REQUEST,
  SET_ORGANIZATION_ACTIVITY_SUCCESS,
  SET_ORGANIZATION_ACTIVITY_FAILURE,
  UPDATE_ORGANIZATION_ACTIVITY_REQUEST,
  UPDATE_ORGANIZATION_ACTIVITY_SUCCESS,
  UPDATE_ORGANIZATION_ACTIVITY_FAILURE,
  RESET_ORGANIZATION_ACTIVITY_REQUEST
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const OrganizationActivity = (state = defaultState, action) => {
  switch (action.type) {
    case GET_ORGANIZATION_ACTIVITY_REQUEST:
    case SET_ORGANIZATION_ACTIVITY_REQUEST:
    case UPDATE_ORGANIZATION_ACTIVITY_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case GET_ORGANIZATION_ACTIVITY_SUCCESS:
    case SET_ORGANIZATION_ACTIVITY_SUCCESS:
    case UPDATE_ORGANIZATION_ACTIVITY_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case GET_ORGANIZATION_ACTIVITY_FAILURE:
    case SET_ORGANIZATION_ACTIVITY_FAILURE:
    case UPDATE_ORGANIZATION_ACTIVITY_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case RESET_ORGANIZATION_ACTIVITY_REQUEST:
      return {
        ...state,
        pendding: false,
        result: null,
      };
    default:
      return state;
  }
};

export default OrganizationActivity;
