import CONFIG from '../../../utility/function/config';

export const LOGIN = 'LOGIN';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const LOGOUT = 'LOGOUT';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';

export const requestLogin = (email, password) => {
    return {
        type: LOGIN,
        promise: { method: 'post', url: `${CONFIG.host}/api/login`, data: { email, password } }
    };
};

export const requestLogout = (jwt) => {
    return {
        type: LOGOUT,
        promise: {
            method: 'POST',
            url: `${CONFIG.host}/api/logout`,
            data: {
                 token: jwt,
            },
        }
    };
};
