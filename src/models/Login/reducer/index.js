import {LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE } from '../action';
import {LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE } from '../action';

const defaultState = {
    result: null,
    pendding: false
};

const Login = (state = defaultState, action) => {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                pendding: true,
                logout: false,
                result: null,
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                pendding: false,
                logout: false,
                result: action.data
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                pendding: false,
                logout: false,
                result: action.data
            };
        case LOGOUT_REQUEST:
            return {
                ...state,
                logout: false,
            };
        case LOGOUT_SUCCESS:
            return {
                ...state,
                logout: true
            };
        case LOGOUT_FAILURE:
            return {
                ...state,
                logout: false
            };
    default:
        return state
    }
}

export default Login;
