import {
  SEARCH_ORGANIZATIONS_REQUEST,
  SEARCH_ORGANIZATIONS_SUCCESS,
  SEARCH_ORGANIZATIONS_FAILURE,
  GET_ORGANIZATION_REQUEST,
  GET_ORGANIZATION_SUCCESS,
  GET_ORGANIZATION_FAILURE,
} from '../action';

const defaultState = {
  result: null,
  pendding: false,
};

const Organizations = (state = defaultState, action) => {
  switch (action.type) {
    case SEARCH_ORGANIZATIONS_REQUEST:
    case GET_ORGANIZATION_REQUEST:
      return {
        ...state,
        pendding: true,
        result: null,
      };
    case SEARCH_ORGANIZATIONS_SUCCESS:
    case GET_ORGANIZATION_SUCCESS:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    case SEARCH_ORGANIZATIONS_FAILURE:
    case GET_ORGANIZATION_FAILURE:
      return {
        ...state,
        pendding: false,
        result: action.data,
      };
    default:
      return state;
  }
};

export default Organizations;
