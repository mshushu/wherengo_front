import CONFIG from '../../../utility/function/config';

export const SEARCH_ORGANIZATIONS = 'SEARCH_ORGANIZATIONS';
export const SEARCH_ORGANIZATIONS_REQUEST = 'SEARCH_ORGANIZATIONS_REQUEST';
export const SEARCH_ORGANIZATIONS_SUCCESS = 'SEARCH_ORGANIZATIONS_SUCCESS';
export const SEARCH_ORGANIZATIONS_FAILURE = 'SEARCH_ORGANIZATIONS_FAILURE';

export const GET_ORGANIZATION = 'GET_ORGANIZATION';
export const GET_ORGANIZATION_REQUEST = 'GET_ORGANIZATION_REQUEST';
export const GET_ORGANIZATION_SUCCESS = 'GET_ORGANIZATION_SUCCESS';
export const GET_ORGANIZATION_FAILURE = 'GET_ORGANIZATION_FAILURE';

const URL = `${CONFIG.host}/api/search/organizations`;

export const searchOrganizations = params => {
  return {
    type: SEARCH_ORGANIZATIONS,
    promise: {
      method: 'POST',
      data: params,
      url: URL,
    },
  };
};

export const getOrganization = id => {
  return {
    type: GET_ORGANIZATION,
    promise: {
      method: 'GET',
      url: URL + `/${id}`,
    },
  };
};
