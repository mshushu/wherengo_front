const reducerInfo = (state = [], action) => {
  switch (action.type) {
    case 'CHANGE_INFO':
      return [
          ...state,
          action.info
      ];
    default:
      return state
  }
}

export default reducerInfo;
