export const changeInfo = (info) => ({
  type: 'CHANGE_INFO',
  info: info
})
