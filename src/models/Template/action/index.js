export const PLUS_NUMBER = 'PLUS_NUMBER';
export const MINUS_NUMBER = 'MINUS_NUMBER';

export const plusNumber = (number = 0) => ({
  type: 'PLUS_NUMBER',
  number: number
})

export const minusNumber = (number = 0) => ({
  type: 'MINUS_NUMBER',
  number: number
})
