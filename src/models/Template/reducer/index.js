import {
    PLUS_NUMBER,
    MINUS_NUMBER
} from '../action';

const defaultState = {
    count: 0
};

const Template = (state = defaultState, action) => {
  switch (action.type) {
    case PLUS_NUMBER:
      return {
          ...state,
          count: state.count + Number(action.number)
      };
    case MINUS_NUMBER:
        return {
            ...state,
            count: state.count - Number(action.number)
        };
    default:
      return state
  }
}

export default Template;
