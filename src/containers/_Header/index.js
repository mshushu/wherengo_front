import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import MenuButton from '../../components/MenuButton';

import './_style.scss';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuItems: [
        {
          name: 'ホーム',
          link: '/',
        },
        {
          name: '検索する',
          link: '/Search/Country',
        },
        {
          name: "Wherengoについて",
          link: '/Aboutus',
        },
        {
          name: '寄付について',
          link: '/AboutDonation',
        },
      ],
    };
  }
  componentWillMount = () => {};
  render = () => {
    return (
      <header className="header">
        <a href='/' >
            <img
              src={require('../../assets/images/WhereN’GO_Logo.png')}
              className="logo"
            />
        </a>
        <ul>
          {this.state.menuItems.map((item, index) => (
            <li key={index} className="header__menu_button">
              <MenuButton name={item.name} link={item.link} />
            </li>
          ))}
        </ul>
      </header>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(Header);
