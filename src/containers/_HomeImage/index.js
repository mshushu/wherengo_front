import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import MenuButton from '../../components/MenuButton';

import Snap from 'snapsvg-cjs';

import './_style.scss';

class Image extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuItems: [
        {
          name: 'ホーム',
          link: 'xxx/xxx/xx',
        },
        {
          name: '検索する',
          link: 'xxx/xxx/xx',
        },
        {
          name: 'Wherengoについて',
          link: 'xxx/xxx/xx',
        },
        {
          name: '寄付について',
          link: 'xxx/xxx/xx',
        },
      ],
    };
  }

  svgRender() {
    let element = Snap(this.svgDiv);
    Snap.load(require('../../assets/images/top_image.svg'), function(data) {
      if (element) {
        element.append(data);
        let elem = element.select('#earth_bg').parent();
        let bbox = elem.getBBox();
        let animateMatrix = new Snap.Matrix();
        elem.animate(
          { transform: 'r60, ' + bbox.cx + ', ' + bbox.cy + '  s1,1' },
          1000
        );
      }
    });
  }

  componentWillMount = () => {};
  render = () => {
    return (
      <div className="image_box">
        <div className="image__top_image" ref={d => (this.svgDiv = d)} />
      </div>
    );
  };
  componentDidMount = () => {
    this.svgRender();
  };
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {
    this.svgRender();
  };
}

// export default AdminLoginPageComponent;
export default connect()(Image);
