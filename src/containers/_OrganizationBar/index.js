import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

//import StepItem from '../../components/StepItem';

import StepItem from '../../components/StepItem';

import './_style.scss';

class OrganizationBar extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const currentpage = window.location.pathname;

    const { organization } = this.props;
    if (!organization || !organization.organization_activities) {
      return '';
    }

    const activity = organization.organization_activities;

    let areaTitle = '';
    if (activity.organization_activity_world_areas.length > 1) {
      areaTitle = 'インターナショナル';
    } else if (
      activity.organization_activity_japan_areas.length > 0 ||
      activity.organization_activity_world_areas[0].world_areas.name == '日本'
    ) {
      areaTitle = '日本';
    }

    let activityTypes = [];
    let activityTypeIcon = '';
    if (
      activity.organization_activity_types &&
      activity.organization_activity_types.length > 0
    ) {
      activity.organization_activity_types.map((activityType, index) => {
        activityTypes.push({
          name: activityType.activity_types.name,
          icon: activityType.activity_types.icon,
        });
      });
    }

    let organizationUrl = null;
    if (organization.web_page_url) {
      organizationUrl = organization.web_page_url;
    }

    return (
      <div className="organizationbar">
        <div className="organizationbar__innerbox">
          <div className="organizationbar__leftitem">
            {areaTitle ? (
              <div className="organizationbar__badge organizationbar__badge--international">
                <i className="material-icons organizationbar__badgeicon">
                  language
                </i>
                <p className="organizationbar__badgetitle">{areaTitle}</p>
              </div>
            ) : (
              ''
            )}
            {activityTypes
              ? activityTypes.map((activityType, index) => (
                  <div
                    key={index}
                    className="organizationbar__badge organizationbar__badge--refugee">
                    <i className="material-icons organizationbar__badgeicon">
                      {activityType.icon}
                    </i>
                    <p className="organizationbar__badgetitle">
                      {activityType.name}
                    </p>
                  </div>
                ))
              : ''}
          </div>
          <div className="organizationbar__centeritem">
            <h1>{activity.name || ''}</h1>
          </div>
          <div className="organizationbar__rightitem">
            {organizationUrl ? (
              <div className="organizationbar__urlBox">
                <p className="organizationbar__urlTitle">団体のURL:</p>
                <a href={organizationUrl} className="organizationbar__url">
                  {organizationUrl}
                </a>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(OrganizationBar);
