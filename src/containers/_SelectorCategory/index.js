import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import CategoryItem from '../../components/CategoryItem';
import Checkbox from 'material-ui/Checkbox';

import './_style.scss';

class SelectorCategory extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const {
      ActivityType,
      changeSelectedCategory,
      selectedCategory,
      changeAllSelectedCategory,
      selectedAllCategoryIsChecked,
    } = this.props;
    const dataList = ActivityType.result ? ActivityType.result.data : [];
    const list = this._listToMatrix(dataList, 3);

    return (
      <div className="selector_category">
        <div className="selector_category__innerpanel">
          <p className="selector_category__description">
            活動のカテゴリーを選択してください。
          </p>
          <Checkbox
            className="selector_category__allselect"
            label="全てを選択"
            onCheck={changeAllSelectedCategory}
            data-list={JSON.stringify(dataList)}
            checked={selectedAllCategoryIsChecked}
          />
          <div className="row">
            {list.map((itemList, index) => (
              <div key={index} className="row">
                {itemList.map(item => (
                  <CategoryItem
                    icon={item.icon}
                    key={item.seq}
                    name={item.name}
                    seq={item.seq}
                    selectedCategory={selectedCategory}
                    changeSelectedCategory={changeSelectedCategory}
                  />
                ))}
              </div>
            ))}
          </div>
          <a href="/Search/Country" className="backButton">
            一つ前に戻る
          </a>
        </div>
      </div>
    );
  };

  _listToMatrix = (list, elementsPerSubArray) => {
    var matrix = [],
      i,
      k;

    for (i = 0, k = -1; i < list.length; i++) {
      if (i % elementsPerSubArray === 0) {
        k++;
        matrix[k] = [];
      }

      matrix[k].push(list[i]);
    }

    return matrix;
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(SelectorCategory);
