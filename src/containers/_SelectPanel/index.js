import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import SelectBlock from '../../components/SelectBlock';
import NextButton from '../../components/NextButton';

import './_style.scss';

class SelectPanel extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const {
      selectedCountry,
      selectedCategory,
      link,
      nextButtonTitle,
      changeSelectedCountry,
      changeSelectedCategory,
      map,
    } = this.props;
    const selectedCountryList = (selectedCountry.world || []).concat(
      selectedCountry.jp || []
    );
    return (
      <div className="select_panel">
        <SelectBlock
          selectedCountryWorld={selectedCountry.world || []}
          selectedCountryJapan={selectedCountry.jp || []}
          changeSelected={changeSelectedCountry}
          title="選択された場所"
        />
        <SelectBlock
          selectedCategory={selectedCategory || []}
          changeSelected={changeSelectedCategory}
          title="選択されたカテゴリー"
        />
        <NextButton link={link} name={nextButtonTitle} />
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(SelectPanel);
