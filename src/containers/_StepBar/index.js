import React from 'react';
import { Link } from 'react-router'

import { connect } from 'react-redux';

//import StepItem from '../../components/StepItem';

import StepItem from '../../components/StepItem';

import './_style.scss';


class StepBar extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount = () =>{
    }
    render = () => {
        const currentpage = window.location.pathname;

        return (
            <div className={this.props.className + " stepbar"}>
                <div className='stepbar__innerbox'>
                    <StepItem number='1' name='活動場所を選択' active={currentpage == '/Search/Country'? "true" : "false" }/>
                    <StepItem number='2' name='カテゴリーを選択' active={currentpage == '/Search/Category'? "true" : "false" }/>
                    <StepItem number='3' name='検索結果' active={currentpage == '/Search/Result'? "true" : "false" }/>
                    <span className='stepbar__bar'></span>
                </div>
            </div>
        );
    }
    componentDidMount = () => {
    }
    componentWillUnmount = () => {
    }
    componentWillReceiveProps = (nextProps) => {
    }
    shouldComponentUpdate = (nextProps, nextState) => {
        return true;
    }
    componentWillUpdate = (nextProps, nextState) => {
    }
    componentDidUpdate = (prevProps, prevState) => {
    }

};



// export default AdminLoginPageComponent;
export default connect()(StepBar);
