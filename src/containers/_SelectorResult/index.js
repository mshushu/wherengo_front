import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';

import OrganizationItem from '../../components/OrganizationItem';

import CONFIG from '../../utility/function/config';

import './_style.scss';

class SelectorResult extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const { Organizations } = this.props;
    let organizationList = [];
    if (
      Organizations.result &&
      Organizations.result.data &&
      Organizations.result.data.organizations
    ) {
      organizationList = Organizations.result.data.organizations;
    }

    return (
      <div className="organization_container">
        <div className="organization_container__innerbox">
          <div className="organization_container__row">
            {organizationList.map((item, index) => (
              <OrganizationItem
                key={index}
                seq={item.seq}
                thumbnail={
                  item.organization_activities.organization_activity_image
                    ? `${CONFIG.host}/images/organization/${
                        item.seq
                      }/activity/${
                        item.organization_activities.organization_activity_image
                      }`
                    : null
                }
                title={item.organization_activities.name}
                detail={item.organization_activities.intro}
                types={item.organization_activities.organization_activity_types}
              />
            ))}
          </div>
          <Link to={'/Search/Category'} className="backButton">
            一つ前に戻る
          </Link>
        </div>
      </div>
    );
  };
  tapMap(map) {
    this.setState({
      map: map,
    });
  }
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(SelectorResult);
