import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import OrganizationItem from '../../components/OrganizationItem';
import OrganizationTab from '../../components/OrganizationTab';
import './_style.scss';

import Checkbox from 'material-ui/Checkbox';
import CONFIG from '../../utility/function/config';
import noImage from '../../assets/images/no_image.jpeg';

class SelectorOrganization extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      panel: 'detail',
    };
    this.tapPanel = this.tapPanel.bind(this);
  }
  componentWillMount = () => {};
  render = () => {
    const panelClassDetail =
      this.state.panel == 'detail'
        ? 'organizationContainer__innerbox detailPanel'
        : 'organizationContainer__innerbox detailPanel hidden';
    const panelClassProjects =
      this.state.panel == 'projects'
        ? 'organizationContainer__innerbox projectsPanel'
        : 'organizationContainer__innerbox projectsPanel hidden';
    const panelClassBasicInfo =
      this.state.panel == 'basicinfo'
        ? 'organizationContainer__innerbox basicInfoPanel'
        : 'organizationContainer__innerbox basicInfoPanel hidden';
    const { organization } = this.props;

    if (!organization || !organization.organization_activities) {
      return '';
    }

    const imgPath = `${CONFIG.host}/images/organization/${
      organization.seq
    }/activity/`;
    const projectImagePath = `${CONFIG.host}/images/organization/${
      organization.seq
    }/project/`;
    const activity = organization.organization_activities;

    const areas = (activity.organization_activity_world_areas || []).concat(
      activity.organization_activity_japan_areas || []
    );

    return (
      <div className="organizationContainer">
        <OrganizationTab tapPanel={this.tapPanel} panel={this.state.panel} />
        <div className={panelClassDetail}>
          <div className="organizationContainer__row">
            <div className="organizationContainer__rightContent">
              <h2 className="detailPanel__header">この団体は？</h2>
              <p className="detailPanel__bigText">
                {activity.intro ? activity.intro : ''}
              </p>
            </div>

            <img
              className="detailPanel__mainImage"
              src={
                activity.organization_activity_image
                  ? imgPath + activity.organization_activity_image
                  : noImage
              }
            />
          </div>

          <div className="organizationContainer__row organizationContainer__row--noflex">
            <h2 className="detailPanel__header">ポイント：</h2>
            <ul className="detailPanel__list">
              {activity.feature1 ? (
                <li className="detailPanel__listItems">{activity.feature1}</li>
              ) : (
                ''
              )}
              {activity.feature2 ? (
                <li className="detailPanel__listItems">{activity.feature2}</li>
              ) : (
                ''
              )}
              {activity.feature3 ? (
                <li className="detailPanel__listItems">{activity.feature3}</li>
              ) : (
                ''
              )}
            </ul>
          </div>

          <div className="organizationContainer__row">
            <div className="organizationContainer__rowBlock organizationContainer__rowBlock--right">
              <h2 className="detailPanel__header">活動分野</h2>
              {activity.organization_activity_detail_types ? (
                <div className="organizationContainer__innerBlock">
                  {activity.organization_activity_types.map((item, index) => (
                    <p key={index} className="detailPanel__fieldBadge">
                      {item.activity_types.name}
                    </p>
                  ))}
                  {activity.organization_activity_detail_types.map(
                    (item, index) => (
                      <p key={index} className="detailPanel__fieldBadge">
                        {item.activity_detail_types.name}
                      </p>
                    )
                  )}
                </div>
              ) : (
                ''
              )}
            </div>

            <div className="organizationContainer__rowBlock">
              <h2 className="detailPanel__header">活動エリア：</h2>
              {areas.length > 0 ? (
                <div className="organizationContainer__innerBlock">
                  {areas.map((item, index) => (
                    <p key={index} className="detailPanel__fieldArea">
                      {item.world_areas
                        ? item.world_areas.name
                        : item.japan_areas.name}
                    </p>
                  ))}
                </div>
              ) : (
                ''
              )}
            </div>
          </div>
          <div className="organizationContainer__row organizationContainer__row--noflex">
            <h2 className="detailPanel__header">主な活動実績：</h2>
            <div className="organizationContainer__row">
              {activity.activity1_intro || activity.activity1_image ? (
                <div className="activityItem">
                  <img
                    className="activityItem__image"
                    src={
                      activity.activity1_image
                        ? imgPath + activity.activity1_image
                        : noImage
                    }
                  />
                  {activity.activity1_intro ? (
                    <div className="activityItem__description">
                      <p className="activityItem__text">
                        {activity.activity1_intro}
                      </p>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
              ) : (
                ''
              )}

              {activity.activity2_intro || activity.activity2_image ? (
                <div className="activityItem">
                  <img
                    className="activityItem__image"
                    src={
                      activity.activity2_image
                        ? imgPath + activity.activity2_image
                        : noImage
                    }
                  />
                  {activity.activity2_intro ? (
                    <div className="activityItem__description">
                      <p className="activityItem__text">
                        {activity.activity2_intro}
                      </p>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
              ) : (
                ''
              )}

              {activity.activity3_intro || activity.activity3_image ? (
                <div className="activityItem">
                  <img
                    className="activityItem__image"
                    src={
                      activity.activity3_image
                        ? imgPath + activity.activity3_image
                        : noImage
                    }
                  />
                  {activity.activity3_intro ? (
                    <div className="activityItem__description">
                      <p className="activityItem__text">
                        {activity.activity3_intro}
                      </p>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
              ) : (
                ''
              )}
            </div>
          </div>

          <div className="organizationContainer__row organizationContainer__row--noflex">
            <h2 className="detailPanel__header">プロジェクト：</h2>

            <div className="organizationContainer__row">
              {organization.organization_activities.project1_title ||
              organization.organization_activities.project1_intro ? (
                <div className="projectItem">
                  <h3 className="projectItem__title">
                    {organization.organization_activities.project1_title}
                  </h3>
                  <div className="projectItem__content">
                    <div className="projectItem__description">
                      <p className="projectItem__text">
                        {organization.organization_activities.project1_intro}
                      </p>
                      <a
                        className="projectItem__link"
                        onClick={() => {
                          this.tapPanel('projects');
                        }}>
                        詳細へ
                      </a>
                    </div>
                  </div>
                </div>
              ) : (
                ''
              )}

              {organization.organization_activities.project2_title ||
              organization.organization_activities.project2_intro ? (
                <div className="projectItem">
                  <h3 className="projectItem__title">
                    {organization.organization_activities.project2_title}
                  </h3>
                  <div className="projectItem__content">
                    <div className="projectItem__description">
                      <p className="projectItem__text">
                        {organization.organization_activities.project2_intro}
                      </p>
                      <a
                        className="projectItem__link"
                        onClick={() => {
                          this.tapPanel('projects');
                        }}>
                        詳細へ
                      </a>
                    </div>
                  </div>
                </div>
              ) : (
                ''
              )}

              {organization.organization_activities.project3_title ||
              organization.organization_activities.project3_intro ? (
                <div className="projectItem">
                  <h3 className="projectItem__title">
                    {organization.organization_activities.project3_title}
                  </h3>
                  <div className="projectItem__content">
                    <div className="projectItem__description">
                      <p className="projectItem__text">
                        {organization.organization_activities.project3_intro}
                      </p>
                      <a
                        className="projectItem__link"
                        onClick={() => {
                          this.tapPanel('projects');
                        }}>
                        詳細へ
                      </a>
                    </div>
                  </div>
                </div>
              ) : (
                ''
              )}
            </div>
          </div>
        </div>
        <div className={panelClassProjects}>
          {organization.organization_projects &&
          organization.organization_projects.length > 0
            ? organization.organization_projects.map((project, index) => (
                <div key={index} className="project">
                  <div className="projectDetail">
                    <div>
                      {project.name ? (
                        <p className="projectDetail__title">{project.name}</p>
                      ) : (
                        ''
                      )}
                      {project.start_date || project.end_date ? (
                        <p className="projectDetail__date">
                          実施期間：
                          {`${project.start_date.split('-')[0]}年${
                            project.start_date.split('-')[1]
                          }月`}
                          {project.end_date &&
                          new Date(project.end_date) > new Date()
                            ? '〜実施中'
                            : `〜${project.end_date.split('-')[0]}年${
                                project.end_date.split('-')[1]
                              }月`}
                        </p>
                      ) : (
                        ''
                      )}
                      <div className="projectDetail__row">
                        <p className="projectDetail__areaTitle">エリア：</p>
                        <div className="projectDetail__innerRow">
                          {(project.organization_project_world_areas || [])
                            .concat(
                              project.organization_project_japan_areas || []
                            )
                            .map((item, index) => (
                              <p
                                key={index}
                                className="projectDetail__areaBadge">
                                {item.world_areas
                                  ? item.world_areas.name
                                  : item.japan_areas.name}
                              </p>
                            ))}
                        </div>
                      </div>
                      {project.organization_project_categories ? (
                        <div className="projectDetail__row">
                          <p className="projectDetail__categoryTitle">
                            カテゴリー：
                          </p>
                          <div className="projectDetail__innerRow">
                            {project.organization_project_categories.map(
                              (item, index) =>
                                item.project_categories ? (
                                  <p
                                    key={index}
                                    className="projectDetail__categoryBadge">
                                    {item.project_categories.name}
                                  </p>
                                ) : (
                                  ''
                                )
                            )}
                          </div>
                        </div>
                      ) : (
                        ''
                      )}
                      {project.content ? (
                        <p className="projectDetail__description">
                          {project.content}
                        </p>
                      ) : (
                        ''
                      )}
                    </div>
                    <img
                      className="projectDetail__image"
                      src={
                        project.image
                          ? projectImagePath + project.image
                          : noImage
                      }
                    />
                  </div>

                  {project.reason || project.result ? (
                    <div className="projectBottomDetail">
                      <div className="projectBackground">
                        <div className="projectBackground__innerbox">
                          <p className="projectBackground__title">問題背景</p>
                          {project.reason ? (
                            <p className="projectBackground__description">
                              {project.reason}
                            </p>
                          ) : (
                            ''
                          )}
                        </div>
                      </div>
                      <div className="projectResult">
                        <div className="projectResult__innerbox">
                          <p className="projectResult__title">結果</p>
                          {project.result ? (
                            <p className="projectResult__description">
                              {project.result}
                            </p>
                          ) : (
                            ''
                          )}
                        </div>
                      </div>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
              ))
            : ''}
        </div>
        <div className={panelClassBasicInfo}>
          <div className="basicInfo">
            <h2 className="basicInfo__title">基本情報</h2>
            <div className="basicInfo__row">
              <p className="basicInfo__infoTitle">所在地：</p>
              <p className="basicInfo__infoContent">
                {organization.address ? organization.address : ''}
              </p>
            </div>

            <div className="basicInfo__row">
              <p className="basicInfo__infoTitle">代表者名：</p>
              <p className="basicInfo__infoContent">
                {organization.representative_name
                  ? organization.representative_name
                  : ''}
              </p>
            </div>

            <div className="basicInfo__row">
              <p className="basicInfo__infoTitle">法人の種類：</p>
              <p className="basicInfo__infoContent">
                {organization.organization_kinds &&
                organization.organization_kinds.kind
                  ? organization.organization_kinds.kind
                  : ''}
              </p>
            </div>

            <div className="basicInfo__row">
              <p className="basicInfo__infoTitle">連絡先：</p>
              <p className="basicInfo__infoContent">
                {organization.tel ? `TEL:${organization.tel}` : ''}
                {organization.email ? ` Email:${organization.email}` : ''}
              </p>
            </div>
            <h2 className="basicInfo__title">財務情報</h2>
            <div className="basicInfo__row">
              <p className="basicInfo__infoTitle">収入：</p>
              <p className="basicInfo__infoBar basicInfo__infoBar--income" />
              <p className="basicInfo__infoAmount basicInfo__infoAmount--income" />
            </div>
            <div className="basicInfo__row">
              <p className="basicInfo__infoTitle">支出：</p>
              <p className="basicInfo__infoBar basicInfo__infoBar--spending" />
              <p className="basicInfo__infoAmount basicInfo__infoAmount--spending" />
            </div>
            {organization.organization_donation_type ? (
              <div className="basicInfo">
                <h2 className="basicInfo__donationTitle">寄付方法</h2>
                <div className="basicInfo__row">
                  {organization.organization_donation_type.map(
                    (item, index) => (
                      <div key={index}>
                        <Checkbox
                          key={index}
                          label={item.donation_types.type}
                          className="basicInfo__label"
                          checked={true}
                        />
                        {item.detail ? `:${item.detail}` : ''}
                      </div>
                    )
                  )}
                </div>
              </div>
            ) : (
              ''
            )}
          </div>
          {organization.donation_page_url ? (
            <a
              className="basicInfo__button"
              href={organization.donation_page_url}
              target="_blank">
              詳しくはこちらのページでご覧ください。
            </a>
          ) : (
            ''
          )}
        </div>
      </div>
    );
  };
  tapPanel(panel) {
    this.setState({
      panel: panel,
    });
  }
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPanelComponent;
export default connect()(SelectorOrganization);
