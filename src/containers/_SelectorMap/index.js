import React from 'react';
import { Link } from 'react-router';

import Checkbox from 'material-ui/Checkbox';
import MapTab from '../../components/MapTab';
import MapLabelCheck from '../../components/MapLabelCheck';

import './_style.scss';

class SelectorMap extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const {
      map,
      tapMap,
      setsSlectedAllJapanIsChecked,
      selectedAllJapanIsChecked,
      changeAllSelectedJapan,
      setsSlectedAllWorldIsChecked,
      selectedAllWorldIsChecked,
      changeAllSelectedWorld,
    } = this.props;
    let japanArea = null;
    if (this.props.JapanArea.result && this.props.JapanArea.result.data) {
      japanArea = this.props.JapanArea.result.data;
    }

    let worldArea = null;
    if (this.props.WorldArea.result && this.props.WorldArea.result.data) {
      worldArea = this.props.WorldArea.result.data;
    }

    const imgClassJp =
      map == 'jp' ? 'map_selecotr__japanmap' : 'map_selecotr__japanmap hidden';
    const imgClassWorld =
      map == 'world'
        ? 'map_selecotr__worldmap'
        : 'map_selecotr__worldmap hidden';

    const labelList = map == 'jp' ? japanArea : worldArea;

    const dataList =
      map == 'jp' ? (japanArea ? japanArea : []) : worldArea ? worldArea : [];

    return (
      <div className="map_selector_outer">
        <div className="map_selector">
          <Checkbox
            className="selector_category__allselect"
            label="全てを選択"
            data-list={JSON.stringify(dataList)}
            onCheck={
              map == 'jp' ? changeAllSelectedJapan : changeAllSelectedWorld
            }
            checked={
              map == 'jp'
                ? selectedAllJapanIsChecked
                : selectedAllWorldIsChecked
            }
          />
          <MapTab tapMap={tapMap} map={map} />
          {labelList
            ? labelList.map((item, index) => (
                <MapLabelCheck
                  key={index}
                  class={item.class_name}
                  seq={item.seq}
                  name={item.name}
                  map={map}
                  selectedCountry={this.props.selectedCountry}
                  changeSelectedCountry={this.props.changeSelectedCountry}
                />
              ))
            : null}
          <img
            className={imgClassJp}
            src={require('../../assets/images/japan_map.svg')}
            lang="ja"
            alt="日本エリア"
          />
          <img
            className={imgClassWorld}
            src={require('../../assets/images/world_map_bg.svg')}
            lang="ja"
            alt="世界エリア"
          />
        </div>
      </div>
    );
  };

  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default SelectorMap;
