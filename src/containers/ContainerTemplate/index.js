import React from 'react';
import ComponentTemplate from '../../components/ComponentTemplate';

import './_style.scss';

class ContainerTemplate extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
    }
    render() {
        return (
            <div>
              <h1>This is containers component!</h1>
              <ComponentTemplate/>
            </div>
        );
    }
    componentDidMount() {
    }
    componentWillUnmount() {
    }
    componentWillReceiveProps(nextProps) {
    }
    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    componentWillUpdate(nextProps, nextState) {
    }
    componentDidUpdate(prevProps, prevState) {
    }
};

export default ContainerTemplate;
