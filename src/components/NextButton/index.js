import React from 'react';
import { Link } from 'react-router-dom'

import { connect } from 'react-redux';

import './_style.scss';

class NextButton extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount = () =>{
    }
    render = () => {
        return (
            <Link to={this.props.link} className='next_button'>{this.props.name}</Link>
        );
    }
    componentDidMount = () => {
    }
    componentWillUnmount = () => {
    }
    componentWillReceiveProps = (nextProps) => {
    }
    shouldComponentUpdate = (nextProps, nextState) => {
        return true;
    }
    componentWillUpdate = (nextProps, nextState) => {
    }
    componentDidUpdate = (prevProps, prevState) => {
    }

};

// export default AdminLoginPageComponent;
export default connect()(NextButton);
