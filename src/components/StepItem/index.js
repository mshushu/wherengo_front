import React from 'react';
import { Link } from 'react-router'

import { connect } from 'react-redux';
import Checkbox from 'material-ui/Checkbox';

import './_style.scss';

class StepItem extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount = () =>{
    }
    render = () => {
        return (
            <div className="stepitem">
                <div className={'stepitem__step_circle' + (this.props.active == 'true' ? ' active' : '')}>
                    {this.props.number}
                </div>
                <p>{this.props.name}</p>
            </div>
        );
    }
    componentDidMount = () => {
    }
    componentWillUnmount = () => {
    }
    componentWillReceiveProps = (nextProps) => {
    }
    shouldComponentUpdate = (nextProps, nextState) => {
        return true;
    }
    componentWillUpdate = (nextProps, nextState) => {
    }
    componentDidUpdate = (prevProps, prevState) => {
    }

};

// export default AdminLoginPageComponent;
export default connect()(StepItem);
