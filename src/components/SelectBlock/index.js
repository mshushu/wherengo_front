import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import SelectedItem from '../../components/SelectedItem';

import './_style.scss';

class SelectBlock extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const {
      changeSelected,
      selectedCountryWorld,
      selectedCountryJapan,
      selectedCategory,
      map,
      title,
    } = this.props;
    return (
      <div className="select_block">
        <div className="select_panel__header">
          <p className="select_panel__text">{title}</p>
        </div>
        {(selectedCountryWorld || []).map(
          (item, index) => (
            <SelectedItem
              key={index}
              name={item.name}
              seq={item.seq}
              map="world"
              changeSelected={changeSelected}
            />
          ),
          this
        )}
        {(selectedCountryJapan || []).map(
          (item, index) => (
            <SelectedItem
              key={index}
              name={item.name}
              seq={item.seq}
              map="jp"
              changeSelected={changeSelected}
            />
          ),
          this
        )}
        {(selectedCategory || []).map(
          (item, index) => (
            <SelectedItem
              key={index}
              name={item.name}
              seq={item.seq}
              changeSelected={changeSelected}
            />
          ),
          this
        )}
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(SelectBlock);
