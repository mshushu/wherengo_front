import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import './_style.scss';

class MapTab extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const worldMapClass = this.props.map == 'world' ? 'active' : '';
    const jpMapClass = this.props.map == 'jp' ? 'active' : '';

    return (
      <ul to={this.props.link} className="map_tab">
        <li className="map_tab__items_left">
          <a
            className={worldMapClass}
            onClick={() => {
              this.props.tapMap('world');
            }}>
            世界地図
          </a>
        </li>
        <li className="map_tab__items_right">
          <a
            className={jpMapClass}
            onClick={() => {
              this.props.tapMap('jp');
            }}>
            日本詳細
          </a>
        </li>
      </ul>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(MapTab);
