import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';
import Checkbox from 'material-ui/Checkbox';
import FontIcon from 'material-ui/FontIcon';

import './_style.scss';

class CategoryItem extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const {
      name,
      seq,
      changeSelectedCategory,
      icon,
      selectedCategory,
    } = this.props;

    let index = -1;
    for (let i = 0; i < selectedCategory.length; i++) {
      const { seq, name } = selectedCategory[i];
      if (this.props.seq == seq && this.props.name == name) {
        index = i;
      }
    }

    return (
      <div className="category-item__label ">
        <Checkbox
          className="category-item__checkbox"
          label={name}
          data-seq={seq}
          data-name={name}
          data-icon={icon}
          labelPosition="left"
          onCheck={changeSelectedCategory}
          checked={index != -1}
        />
        <div className="category-item__circle">
          <i className="material-icons category-item__icon">{icon}</i>
        </div>
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(CategoryItem);
