import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';


import './_style.scss';

class NewArrival extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    return (
      <div className="newArrivalBlock">
        <div className="newArrivalBlock__header">
          最新掲載情報
        </div>
        <div className="newArrivalBlock__content">
          <Link to="/Organization/203" className="newArrivalBlock__Link">認定NPO法人 テラ・ルネッサンス</Link>
          <Link to="/Organization/214" className="newArrivalBlock__Link">認定NPO法人 ACE</Link>
          <Link to="/Organization/205" className="newArrivalBlock__Link">公益社団法人チャンス・フォー・チルドレン</Link>
          <Link to="/Organization/142" className="newArrivalBlock__Link">認定NPO法人 カタリバ</Link>
          <Link to="/Organization/141" className="newArrivalBlock__Link">特定非営利活動法人 日本こども支援協会</Link>
          <Link to="/Organization/128" className="newArrivalBlock__Link">認定NPO法人フローレンス</Link>
          <Link to="/Organization/127" className="newArrivalBlock__Link">特定非営利活動法人アクセプト・インターナショナル</Link>
          <Link to="/Organization/163" className="newArrivalBlock__Link">特定非営利活動法人リトルワンズ</Link>
          <Link to="/Organization/130" className="newArrivalBlock__Link">特定非営利活動法人　東京メンタルヘルス・スクエア</Link>
        </div>
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(NewArrival);
