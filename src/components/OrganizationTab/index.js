import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import './_style.scss';

class OrganizationTab extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const detailPanelClass = this.props.panel == 'detail' ? 'active' : '';
    const projectsPanelClass = this.props.panel == 'projects' ? 'active' : '';
    const basicPanelClass = this.props.panel == 'basicinfo' ? 'active' : '';

    return (
      <ul to={this.props.link} className="organizationtab">
        <li className="organizationtab__left">
          <a
            className={detailPanelClass}
            onClick={() => {
              this.props.tapPanel('detail');
            }}>
            活動概要
          </a>
        </li>
        <li className="organizationtab__center">
          <a
            className={projectsPanelClass}
            onClick={() => {
              this.props.tapPanel('projects');
            }}>
            プロジェクト
          </a>
        </li>
        <li className="organizationtab__right">
          <a
            className={basicPanelClass}
            onClick={() => {
              this.props.tapPanel('basicinfo');
            }}>
            基本情報
          </a>
        </li>
      </ul>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(OrganizationTab);
