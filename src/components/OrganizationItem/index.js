import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import Checkbox from 'material-ui/Checkbox';
import FontIcon from 'material-ui/FontIcon';

import noImage from '../../assets/images/no_image.jpeg';
import './_style.scss';

class OrganizationItem extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const { title, detail, seq, thumbnail, types } = this.props;

    return (
      <div className="organization-item">
        <Link
          to={`/Organization/${seq}`}
          className="organization-item__innerlink">
          <div className="">
            <img
              className="organization-item__image"
              src={thumbnail ? thumbnail : noImage}
            />
          </div>
          <div className="organization-item__description">
            <p className="organization-item__title">{title}</p>
            <div className="organization-item__tag-row">
              {(types || []).map((item, index) => (
                <span key={index} className="organization-item__tag-item">
                  {item.activity_types.name}
                </span>
              ))}
            </div>
            <p className="organization-item__text">{detail}</p>
            <div className="organization-item__link">
              <p>もっと見る＞</p>
            </div>
          </div>
        </Link>
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(OrganizationItem);
