import React from 'react';
import './_style.scss';

class ContainerTemplate extends React.Component {
    constructor(props) {
        super(props);
        this.count = this.count.bind(this);

        this.state = {
            counter: 0
        };
    }
    componentWillMount() {
    }
    render() {
        return (
            <div>
                <h2>Counter: {this.state.counter}</h2>
                <p>{this.props.name}</p>
            </div>
        );
    }
    count() {
        this.setState({ counter: this.state.counter + 1 });
    }
    componentDidMount() {
        this.interval = setInterval(this.count, 1000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    componentWillReceiveProps(nextProps) {
    }
    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    componentWillUpdate(nextProps, nextState) {
    }
    componentDidUpdate(prevProps, prevState) {
    }
};

export default ContainerTemplate;
