import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';
import Checkbox from 'material-ui/Checkbox';

import './_style.scss';

class SelectedItem extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const { name, seq, map, changeSelected } = this.props;
    return (
      <div className="selected_item">
        <Checkbox
          label={name}
          data-seq={seq}
          data-name={name}
          data-map={map}
          onCheck={changeSelected}
          checked={true}
        />
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(SelectedItem);
