import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';

import './_style.scss';

class ServiceIntro extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    return (
      <div className="serviceIntroBlock">
        <div className="serviceIntroBlock__image">
          <img src={require('../../assets/images/service_left_image.png')} />
        </div>
        <div className="serviceIntroBlock__container">
          <div className="serviceIntroBlock__container__header">
            <img
              src={require('../../assets/images/service_intro_header_icon.png')}
            />
            <h2>Wherengoとは？</h2>
          </div>
          <div className="serviceIntroBlock__container__contents">
            NGO/NPOの情報を検索し、寄付先を探すことができるポータルサイトです。
            <br />
            あなたの関心のある活動場所と、活動分野から団体を探し、
            <br />
            寄付を通して活動を応援して見ませんか？
            <br />
          </div>
        </div>
        <div className="serviceIntroBlock__image">
          <img src={require('../../assets/images/service_right_image.png')} />
        </div>
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(ServiceIntro);
