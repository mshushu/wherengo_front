import React from 'react';
import { Link } from 'react-router-dom'

import { connect } from 'react-redux';

import './_style.scss';

class MenuButton extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount = () =>{
    }
    render = () => {
        let additionalClassName = "";

        if(window.location.pathname == this.props.link) {
            additionalClassName = "active";
        }else if (window.location.pathname == '/Search/Category' && this.props.link == '/Search/Country') {
            additionalClassName = "active";
        }else if (window.location.pathname == '/Search/Result' && this.props.link == '/Search/Country') {
            additionalClassName = "active";
        }else if (window.location.pathname == '/Organization' && this.props.link == '/Search/Country') {
            additionalClassName = "active";
        }

        return (
            <Link to={this.props.link} className={"menu_button__button_link " + additionalClassName}>{this.props.name}</Link>
        );
    }
    componentDidMount = () => {
    }
    componentWillUnmount = () => {
    }
    componentWillReceiveProps = (nextProps) => {
    }
    shouldComponentUpdate = (nextProps, nextState) => {
        return true;
    }
    componentWillUpdate = (nextProps, nextState) => {
    }
    componentDidUpdate = (prevProps, prevState) => {
    }

};

// export default AdminLoginPageComponent;
export default connect()(MenuButton);
