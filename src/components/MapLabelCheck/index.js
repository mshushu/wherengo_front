import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';
import Checkbox from 'material-ui/Checkbox';

import './_style.scss';

class MapLabelCheck extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const {
      name,
      seq,
      map,
      changeSelectedCountry,
      selectedCountry,
    } = this.props;

    let index = -1;
    for (let i = 0; i < selectedCountry[map].length; i++) {
      const { seq, name } = selectedCountry[map][i];
      if (this.props.seq == seq && this.props.name == name) {
        index = i;
      }
    }

    return (
      <div className={'map_label ' + this.props.class}>
        <Checkbox
          label={name}
          data-seq={seq}
          data-name={name}
          data-map={map}
          onCheck={changeSelectedCountry}
          checked={index != -1}
        />
      </div>
    );
  };

  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(MapLabelCheck);
