function getCookie(name) {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function setCookie(name, value, days) {
  let expires = '';

  if (days) {
    days = 369
  }

  var date = new Date();
  date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
  expires = "; expires=" + date.toGMTString();

  document.cookie = name + "=" + value + expires + "; path=/";
}

function deleteCookie(name) {
    this.setCookie(name, '', -1);
}

export default {
    getCookie,
    setCookie,
    deleteCookie
}
