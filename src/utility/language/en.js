export default {
    Login: "Login",
    EnterYourEmail: "Enter your email",
    Next: "Next",
    CreateAccount: "Create account",
    Password: "Password",
    ForgotPassword: "Forgot password?",
    SignIn: "Sign in",
    Posstgress: "Posstgress",
    InvalidEmailAddress: "Invalid email address",
    EnterAnEmail: "Enter an email",
    CouldNotFindYourEmail: "Couldn't find your email",
    PasswordMustBeOfMinimum6CharactersLength: "Password must be of minimum 6 characters length",
    "FAVE": "FAVE"
}
