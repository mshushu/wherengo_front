import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { Provider } from 'react-redux';
import configureStore from './models/configureStore';
const store = configureStore();

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Theme from './theme';

import LANG from './utility/language/';

import Home from './pages/_1.Home';
import Search from './pages/_2.Search';
import Organization from './pages/_5.Organization';
import Aboutus from './pages/_6.Aboutus';
import AboutDonation from './pages/_7.AboutDonation';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider muiTheme={getMuiTheme(Theme)}>
          <BrowserRouter>
            <Switch>
              <Route
                exact
                path="/"
                component={(props, state, params) => (
                  <Home LANG={LANG} router={{ props, state, params }} />
                )}
              />
              <Route
                path="/Search"
                component={(props, state, params) => (
                  <Search LANG={LANG} router={{ props, state, params }} />
                )}
              />
              <Route
                path="/Organization/:id"
                component={(props, state, params) => (
                  <Organization LANG={LANG} router={{ props, state, params }} />
                )}
              />
              <Route
                path="/Aboutus"
                component={(props, state, params) => (
                  <Aboutus LANG={LANG} router={{ props, state, params }} />
                )}
              />
              <Route
                path="/AboutDonation"
                component={(props, state, params) => (
                  <AboutDonation
                    LANG={LANG}
                    router={{ props, state, params }}
                  />
                )}
              />
            </Switch>
          </BrowserRouter>
        </MuiThemeProvider>
      </Provider>
    );
  }
}
