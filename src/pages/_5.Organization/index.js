import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import Header from '../../containers/_Header';
import OrganizationBar from '../../containers/_OrganizationBar';

import SelectorOrganization from '../../containers/_SelectorOrganization';

import { getOrganization } from '../../models/Organizations/action';

import './_style.scss';

class Organization extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {
    const { id } = this.props.router.props.match.params;
    this.props.dispatch(getOrganization(id));
  };
  render = () => {
    let organization = null;
    if (
      this.props.Organization &&
      this.props.Organization.result &&
      this.props.Organization.result.data &&
      this.props.Organization.result.data.organization
    ) {
      organization = this.props.Organization.result.data.organization;
    }
    return (
      <div>
        <Header />
        <OrganizationBar organization={organization} />
        <SelectorOrganization organization={organization} />
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

function select(state) {
  return {
    Organization: state.Organizations,
  };
}

// export default AdminLoginPageComponent;
export default connect(select)(Organization);
