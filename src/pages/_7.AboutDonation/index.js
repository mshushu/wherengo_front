import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import Header from '../../containers/_Header';
import HomeImage from '../../containers/_HomeImage';

import './_style.scss';

class AboutDonation extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    return (
      <div>
        <Header />
        <section className="BlockA">
          <p className="BlockA__bigText">はじめてみませんか？</p>
          <p className="BlockA__smallText">
            NGO/NPOは、多くの方からの寄付を大きな財政源として、　様々な活動を行っています。
            <br />
            応援したい団体に寄付をすることで、よりよい活動や結果に繋がります。
          </p>
        </section>
        <section className="BlockB">
          <h2 className="BlockB__title">寄付のタイプ</h2>
          <div className="BlockB__row">
            <div className="DonationItem">
              <h3 className="DonationItem__header">マンスリータイプ</h3>
              <p className="DonationItem__subheader">
                毎月定額で寄付をするタイプです。
              </p>
              <ul className="DonationItem__list">
                <li className="DonationItem__listItem">
                  <img
                    className="DonationItem__listIcon"
                    src={require('../../assets/images/check_red.svg')}
                    alt="point one" />
                  安定した活動資金になるため、プロジェクトを計画的、効果的に行うことができます。
                </li>
                <li className="DonationItem__listItem">
                  <img
                    className="DonationItem__listIcon"
                    src={require('../../assets/images/check_red.svg')}
                    alt="point two" />
                  クレジットカード等の引き落としのため、初回登録後は手間がかかりません。
                </li>
              </ul>
            </div>
            <div className="DonationItem DonationItem--tsudo">
              <h3 className="DonationItem__header DonationItem__header--tsudo">
                都度タイプ
              </h3>
              <p className="DonationItem__subheader DonationItem__subheader--tsudo">
                任意の時、任意の金額を寄付するタイプです。
              </p>
              <ul className="DonationItem__list">
                <li className="DonationItem__listItem">
                  <img
                    className="DonationItem__listIcon"
                    src={require('../../assets/images/check_blue.svg')}
                    alt="point one" />
                  自由度が高く、ご自身のお好きなタイミングで寄付をすることができます。
                </li>
              </ul>
            </div>
          </div>
          <div className="BlockB__row">
            <div className="DonationItem DonationItem--project">
              <h3 className="DonationItem__header DonationItem__header--project">
                プロジェクトごとタイプ
              </h3>
              <p className="DonationItem__subheader DonationItem__subheader--project">
                NGO/NPOの行なっている、特定のプロジェクトに寄付ができます。
              </p>
              <ul className="DonationItem__list">
                <li className="DonationItem__listItem">
                  <img
                    className="DonationItem__listIcon"
                    src={require('../../assets/images/check_green.svg')}
                    alt="point one" />
                  寄付金が使われるプロジェクトが明確です。
                </li>
              </ul>
            </div>
            <div className="DonationItem DonationItem--other">
              <h3 className="DonationItem__header DonationItem__header--other">
                その他タイプ
              </h3>
              <p className="DonationItem__subheader DonationItem__subheader--other">
                NGO/NPOごとに、その団体で特有の寄付制度を設けている場合があります。
              </p>
              <ul className="DonationItem__list">
                <li className="DonationItem__listItem">
                  <img
                    className="DonationItem__listIcon"
                    src={require('../../assets/images/check_orange.svg')}
                    alt="point one" />
                  詳しくは各団体のHPをご覧ください。
                </li>
              </ul>
            </div>
          </div>
          <div className="BlockB__row">
            <small className="BlockB__smallText">
              ※各団体で提供する寄付のタイプはことなり、すべての団体ですべてのタイプを提供しているわけではありません。
            </small>
          </div>
        </section>
        <a className="BlockButton" href="/Search/Country">
          まずは団体を検索
        </a>
        <section className="BlockC">
          <h2 className="BlockC__title">
            寄付をしたのにお金が戻る？寄付金控除制度とは
          </h2>
          <p className="BlockC__description">
            寄付金控除とは、各都道府県等の認定を受けた認定NPO法人に寄付をした場合、その寄付額に応じた金額が税金から控除される仕組みです。
          </p>
          <p className="BlockC__description">税額控除方式での場合（※1)</p>
          <img
            className="BlockC__image"
            src={require('../../assets/images/donation_koujo.svg')}
            alt="donation return description image" />
          <div className="BlockC__row">
            <img
              className="BlockC__tsumari"
              src={require('../../assets/images/tsumari.svg')}
              lang="ja" alt="つまり、" />
            <ul className="BlockC__list">
              <li className="BlockC__listItem">
                寄付金の最大５０％は実質戻ってくる！
              </li>
              <li className="BlockC__listItem">減税効果がある！</li>
            </ul>
          </div>
          <ul className="BlockC__subList">
            <li className="BlockC__subListItem">※1 税額控除方式以外に、所得控除方式もあり、場合によっては所得控除を選択すると、控除額が大きくなる場合があります。</li>
            <li className="BlockC__subListItem">※2 住民税(地方税）における控除割合は、寄付額の最大10%ですが各自治体で異なります。ご確認下さい。</li>
            <li className="BlockC__subListItem">※3 年間寄付金額は年間所得の40%が上限、税額控除額は年間所得税の25%等が上限等、控除には条件があります。詳しくはお近くの税務署や国税庁HPでご確認下さい。</li>
          </ul>
        </section>
        <a className="BlockButton" href="/Search/Country">
          今すぐ団体を検索する!!
        </a>
      </div>
    );  
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(AboutDonation);
