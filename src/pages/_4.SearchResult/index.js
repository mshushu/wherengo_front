import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import Header from '../../containers/_Header';
import StepBar from '../../containers/_StepBar';
import SelectorResult from '../../containers/_SelectorResult';

import { searchOrganizations } from '../../models/Organizations/action';

import './_style.scss';

class SearchResult extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {
    const { selectedCategory, selectedCountry } = this.props;

    let country = selectedCountry;
    if (
      selectedCountry['jp'].length == 0 &&
      selectedCountry['world'].length == 0 &&
      localStorage.getItem('selectedCountry')
    ) {
      country = JSON.parse(localStorage.getItem('selectedCountry'));
    } else {
      localStorage.setItem('selectedCountry', JSON.stringify(selectedCountry));
    }

    let category = selectedCategory;
    if (
      selectedCategory.length == 0 &&
      localStorage.getItem('selectedCategory')
    ) {
      category = JSON.parse(localStorage.getItem('selectedCategory'));
    } else {
      localStorage.setItem(
        'selectedCategory',
        JSON.stringify(selectedCategory)
      );
    }
    const params = {
      ActivityCategory: category,
      JapanArea: country.jp,
      WorldArea: country.world,
    };
    this.props.dispatch(searchOrganizations(params));
  };
  render = () => {
    const { Organizations } = this.props;

    return (
      <div className="main">
        <StepBar className="organization" />
        <SelectorResult Organizations={Organizations} />
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

function select(state) {
  return {
    Organizations: state.Organizations,
  };
}

// export default AdminLoginPageComponent;
export default connect(select)(SearchResult);
