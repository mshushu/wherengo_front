import React from 'react';
import { connect } from 'react-redux';

import { plusNumber, minusNumber }from '../../models/Template/action';

import './_style.scss';

class PageReduxTemplate extends React.Component {
  constructor(props) {
    super(props);

    this.plusCount = this.plusCount.bind(this);
    this.minusCount = this.minusCount.bind(this);
  }
  componentWillMount() {
  }
  render() {
    return (
      <div>
        <p>Count: {this.props.Template.count}</p>
        <div>
            <button onClick={this.plusCount}>+</button>
            <button onClick={this.minusCount}>-</button>
        </div>
      </div>
    );
  }

  plusCount() {
      this.props.dispatch(plusNumber(2));
  }
  minusCount() {
      this.props.dispatch(minusNumber(1));
  }

  componentDidMount() {
  }
  componentWillUnmount() {
  }
  componentWillReceiveProps(nextProps) {
  }
  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }
  componentWillUpdate(nextProps, nextState) {
  }
  componentDidUpdate(prevProps, prevState) {
  }
};

function select(state) {
    return {
        Template: state.Template
    };
}

export default connect(select)(PageReduxTemplate);
