import React from 'react';
import ComponentTemplate from '../../../components/ComponentTemplate';
import queryString from 'query-string';
import { Link, NavLink, Route } from 'react-router-dom';

import './_style.scss';

class PageRouterTemplate extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {}
  render() {
    const query = queryString.parse(this.props.location.search);
    const activeStyle = {
      color: 'green',
      fontSize: '2rem',
    };

    return (
      <div>
        <img src={require('../../assets/images/react-logo.png')} />
        <h1 className="prefix-test">This is page router component!</h1>
        <p>This is parameter: {this.props.match.params.name}</p>
        <p>{Object.keys(query).length !== 0 && 'This has query param'}</p>
        <ul>
          <li>
            <NavLink
              exact
              to={'/router'}
              activeStyle={activeStyle}
              activeClassName="active">
              router
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              to={'/router/testname'}
              activeStyle={activeStyle}
              activeClassName="active">
              Router With URL Param
            </NavLink>
          </li>
        </ul>
        <ul>
          <li>
            <Link to={'/router'}>router</Link>
          </li>
          <li>
            <Link to={'/router/testname'}>Router With URL Param</Link>
          </li>
          <li>
            <Link to={'/router?link=test'}>Router With Query Param</Link>
          </li>
          <li>
            <Link to={'/router/testname?link=test'}>
              Router With Both param
            </Link>
          </li>
        </ul>
        <Route exact path="/router" component={ComponentTemplate} />
        <Route
          path="/router/:name"
          component={() => (
            <ComponentTemplate name={this.props.match.params.name} />
          )}
        />
      </div>
    );
  }
  componentDidMount() {}
  componentWillUnmount() {}
  componentWillReceiveProps(nextProps) {}
  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }
  componentWillUpdate(nextProps, nextState) {}
  componentDidUpdate(prevProps, prevState) {}
}

export default PageRouterTemplate;
