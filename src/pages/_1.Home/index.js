import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Lottie from 'react-lottie';
import * as animationData from '../../assets/images/walking_girl/data.json';

import Header from '../../containers/_Header';
import ServiceIntro from '../../components/ServiceIntro';

import CONFIG from '../../utility/function/config';
import OrganizationItem from '../../components/OrganizationItem';

import './_style.scss';

import { getHomeOrganizations } from '../../models/HomeInfomations/action';

import RSSParser from 'rss-parser';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mediumList: undefined,
    };
  }
  componentWillMount = () => {};
  render = () => {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice',
      },
    };

    const { organizations } = this.props;

    let organizationList = undefined;
    if (
      organizations.result &&
      organizations.result.data &&
      organizations.result.data.organizations
    ) {
      organizationList = organizations.result.data.organizations;
    }

    const { mediumList } = this.state;

    console.log(mediumList);

    console.log(mediumList);
    return (
      <div className="main mainFrame">
        <Header />
        <div className="topImage">
          <h1 className="topImage__text">
            あなたの「応援したい」が
            <br />
            きっと見つかる。
          </h1>
          <p className="topImage__subText">
            Wherengoは、NGOやNPOの活動内容や
            寄付についてを調べることができるポータルサイトです。
            <br />
            応援したい団体を見つけて、寄付を通して応援してみませんか？
          </p>
          <a className="topImage__button" href="/Search/Country">
            さあ、探してみよう！
          </a>
          <img
            className="topImage__innerImage"
            src={require('../../assets/images/home_image.svg')}
          />

          <ServiceIntro />
        </div>
        {organizationList && (
          <div className="section">
            <div className="section_header">
              <div className="section_header__divider" />
              <div className="section_header__icon">
                <img src={require('../../assets/images/new_icon.png')} />
              </div>
              <div className="section_header__title">最新掲載情報</div>
              <div className="section_header__divider" />
            </div>
            <div className="section_contents">
              {organizationList.map((item, index) => (
                <div key={index} className="section_contents__item">
                  <OrganizationItem
                    seq={item.seq}
                    thumbnail={
                      item.organization_activities.organization_activity_image
                        ? `${CONFIG.host}/images/organization/${
                            item.seq
                          }/activity/${
                            item.organization_activities
                              .organization_activity_image
                          }`
                        : null
                    }
                    title={item.organization_activities.name}
                    types={
                      item.organization_activities.organization_activity_types
                    }
                  />
                </div>
              ))}
            </div>
          </div>
        )}
        {false && (
          <div className="section">
            <div className="section_header">
              <div className="section_header__icon" />
              <div className="section_header__title">
                おすすめのこんてんつはこちら！
              </div>
            </div>
            <div className="section_contents">
              {mediumList.map((item, index) => (
                <a href={item.link} target="_blank">
                  <div>
                    <img src={item.image.url} alt={item.image.title} />
                  </div>
                  <div>
                    <div>{item.title}</div>
                  </div>
                </a>
              ))}
            </div>
          </div>
        )}
        <div className="bottom_container">
          <Link
            className="bottom_container__search_button"
            to="/Search/Country">
            <img src={require('../../assets/images/search_button.png')} />
          </Link>
        </div>
      </div>
    );
  };
  componentDidMount = () => {
    this.props.dispatch(getHomeOrganizations());
    this._loadMedium();
  };

  _loadMedium = () => {
    const self = this;

    const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/';

    const parser = new RSSParser();
    parser.parseURL(
      CORS_PROXY + 'https://medium.com/feed/@wherengo.info',
      function(err, feed) {
        let mediumList = [];
        feed.items.map((item, index) => {
          if (index > 9) {
            return false;
          }

          mediumList.push({
            title: item.title,
            link: item.link,
            image: feed.image,
          });
        });

        self.setState({
          mediumList,
        });
      }
    );
  };

  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
function select(state) {
  return {
    organizations: state.HomeInfomations,
  };
}
export default connect(select)(Home);
