import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import Header from '../../containers/_Header';
import StepBar from '../../containers/_StepBar';
import SelectPanel from '../../containers/_SelectPanel';
import SelectorCategory from '../../containers/_SelectorCategory';

import './_style.scss';

class SearchCategory extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {
    const {
      selectedCountry,
      selectedCategory,
      setSelectedCountry,
      setSelectedCategory,
    } = this.props;

    if (
      selectedCountry['jp'].length == 0 &&
      selectedCountry['world'].length == 0 &&
      localStorage.getItem('selectedCountry')
    ) {
      const country = JSON.parse(localStorage.getItem('selectedCountry'));
      setSelectedCountry(country);
      localStorage.removeItem('selectedCountry');
    }

    if (
      selectedCategory.length == 0 &&
      localStorage.getItem('selectedCategory')
    ) {
      const category = JSON.parse(localStorage.getItem('selectedCategory'));
      setSelectedCategory(category);
      localStorage.removeItem('selectedCategory');
    }
  };
  render = () => {
    const {
      selectedCountry,
      selectedCategory,
      ActivityType,
      changeSelectedCategory,
      changeAllSelectedCategory,
      selectedAllCategoryIsChecked,
    } = this.props;

    return (
      <div className="main mainFrame mainFrameSearch">
        <SelectPanel
          selectedCountry={selectedCountry}
          selectedCategory={selectedCategory}
          changeSelectedCategory={changeSelectedCategory}
          title="選択された場所"
          nextButtonTitle="この内容で検索する"
          link="/Search/Result"
        />
        <StepBar />
        <SelectorCategory
          ActivityType={ActivityType}
          selectedCategory={selectedCategory}
          selectedAllCategoryIsChecked={selectedAllCategoryIsChecked}
          changeSelectedCategory={changeSelectedCategory}
          changeAllSelectedCategory={changeAllSelectedCategory}
        />
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(SearchCategory);
