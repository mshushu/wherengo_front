import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import Header from '../../containers/_Header';
import HomeImage from '../../containers/_HomeImage';

import './_style.scss';

class Aboutus extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    return (
      <div>
        <Header />
        <section className="AboutUs__Block1">
          <h1 className="AboutUs__Block1Header">Wherengoとは</h1>
          <p className="AboutUs__Block1Description">
            NGO/NPOの活動やプロジェクト、寄付について調べられるポータルサイトです。
          </p>
          <p className="AboutUs__Block1Description">
            NGO/NPOはあらゆる社会問題の解決のため、
            <br />
            日本をふくむ世界各国で
            <br />
            プロジェクトを行っています。
          </p>
          <p className="AboutUs__Block1Description">
            あらゆるアクターと連携し
            <br />
            必要なひと、場所に支援を届け、
          </p>
          <p className="AboutUs__Block1Description">
            現状のままでは解決できない問題に
            <br />
            新しい解決策をうみだしています。
          </p>
        </section>
        <a className="AboutUs__BlockButton" href="/search/country">
          まずは団体を検索する。
        </a>
        <img
          className="AboutUs__triangle"
          src={require('../../assets/images/triangle_down_bg.svg')}
          alt="to whereNGO activity"
        />
        <section className="AboutUs__Block2">
          <h2 className="AboutUs__Block2Header">
            Wherengoで、どんな活動があるのか見てみませんか？
          </h2>
          <div className="AboutUs__Block2Row">
            <div className="AboutUs__stepItem AboutUs__stepItem--step1">
              <p className="AboutUs__stepItemTitle AboutUs__stepItemTitle--step1">
                STEP1
              </p>
              <p className="AboutUs__stepItemDescription AboutUs__stepItemDescription--step1">
                興味のある活動エリアを選択する
              </p>
            </div>
            <div className="AboutUs__stepItem AboutUs__stepItem--step2">
              <p className="AboutUs__stepItemTitle AboutUs__stepItemTitle--step2">
                STEP2
              </p>
              <p className="AboutUs__stepItemDescription AboutUs__stepItemDescription--step2">
                関心のあるカテゴリーを選択する
              </p>
            </div>
            <div className="AboutUs__stepItem AboutUs__stepItem--step3">
              <p className="AboutUs__stepItemTitle AboutUs__stepItemTitle--step3">
                STEP3
              </p>
              <p className="AboutUs__stepItemDescription AboutUs__stepItemDescription--step3">
                NGO/NPOの団体情報やプロジェクトをみてみる！
              </p>
            </div>
          </div>
          <div className="AboutUs__Block2Row">
            <img
              className="AboutUs__Block2Image"
              src={require('../../assets/images/Aboutus_image1.svg')}
              lang="ja"
              alt="興味のある活動エリアを選択する"
            />
            <img
              className="AboutUs__Block2Image"
              src={require('../../assets/images/Aboutus_image2.svg')}
              lang="ja"
              alt="関心のあるカテゴリーを選択する"
            />
            <img
              className="AboutUs__Block2Image"
              src={require('../../assets/images/Aboutus_image3.svg')}
              lang="ja"
              alt="NGO/NPOの団体情報やプロジェクトをみてみる！"
            />
          </div>
        </section>
        <a className="AboutUs__BlockButton" href="/search/country">
          早速検索する！
        </a>
        <section className="AboutUs__Block3">
          <h2 className="AboutUs__Block3Header">例えばこんな活動があります</h2>
          <div className="AboutUs__Block3Row">
            <div className="ActivityItem">
              <div className="ActivityItem__header">
                <div className="ActivityItem__titleLeft">関東</div>
                <img
                  className="ActivityItem__titleCenter"
                  src={require('../../assets/images/cross_shape.svg')}
                  lang="ja"
                  alt="かける"
                />
                <div className="ActivityItem__titleRight">子供の貧困</div>
              </div>
              <div className="ActivityItem__description">
                施設で育った子供や、教育を十分に受けられない子供に、あらゆる形の支援を提供しています。
              </div>
              <img
                className="ActivityItem__image"
                src={require('../../assets/images/aboutus_picture1.png')}
                lang="ja"
                alt="安心している子供"
              />
            </div>
            <div className="ActivityItem">
              <div className="ActivityItem__header">
                <div className="ActivityItem__titleLeft">アジア</div>
                <img
                  className="ActivityItem__titleCenter"
                  src={require('../../assets/images/cross_shape.svg')}
                  lang="ja"
                  alt="かける"
                />
                <div className="ActivityItem__titleRight">経済・ビジネス</div>
              </div>
              <div className="ActivityItem__description">
                現地の人々が自立した生活を送れるように、就労支援やビジネスを作り出す支援を行っています
              </div>
              <img
                className="ActivityItem__image"
                src={require('../../assets/images/aboutus_picture4.jpeg')}
                lang="ja"
                alt="安心している子供"
              />
            </div>
          </div>
          <div className="AboutUs__Block3Row">
            <div className="ActivityItem">
              <div className="ActivityItem__header">
                <div className="ActivityItem__titleLeft">アフリカ</div>
                <img
                  className="ActivityItem__titleCenter"
                  src={require('../../assets/images/cross_shape.svg')}
                  lang="ja"
                  alt="かける"
                />
                <div className="ActivityItem__titleRight">難民支援</div>
              </div>
              <div className="ActivityItem__description">
                紛争等により難民となった人々への緊急支援、生活のサポートをしています
              </div>
              <img
                className="ActivityItem__image"
                src={require('../../assets/images/aboutus_picture2.jpg')}
                lang="ja"
                alt="安心している子供"
              />
            </div>
            <div className="ActivityItem">
              <div className="ActivityItem__header">
                <div className="ActivityItem__titleLeft">日本</div>
                <img
                  className="ActivityItem__titleCenter"
                  src={require('../../assets/images/cross_shape.svg')}
                  lang="ja"
                  alt="かける"
                />
                <div className="ActivityItem__titleRight">医療</div>
              </div>
              <div className="ActivityItem__description">
                メンタルヘルスが大きな社会問題になる中、だれもがカウンセリングや悩み相談を利用できるサービスをしています
              </div>
              <img
                className="ActivityItem__image"
                src={require('../../assets/images/aboutus_picture3.jpg')}
                lang="ja"
                alt="安心している子供"
              />
            </div>
          </div>
        </section>
        <section className="AboutUs__Block4">
          <h2 className="AboutUs__Block4Header">寄付をしてみませんか？</h2>
          <p className="AboutUs__Block4Description">
            これらの活動主体のNGO/NPOに寄付をすることで、継続的な活動ができるようになり、さらなるプロジェクトの発展につながります
          </p>
          <img
            className="AboutUs__Block4Image"
            src={require('../../assets/images/Aboutus_process.svg')}
            lang="ja"
            alt="寄付の流れの説明"
          />
          <p className="AboutUs__Block4LastText">さあまずは探してみよう！</p>
        </section>
        <a className="AboutUs__BlockButton" href="/search/country">
          まずは簡単３ステップで検索！
        </a>
        <section className="AboutUs__Block5">
          <h2 className="AboutUs__Block5Header">NGO・NPOとは？</h2>
          <div className="AboutUs__Block5Row">
            <div className="AboutUs__Block5Right">
              <p className="AboutUs__Block5Title">NGO（非政府組織）</p>
              <p className="AboutUs__Block5Description">
                政府によらずにつくられた民間の団体です。一つに国に限らず国境を越えて活動し、政治的状況に関わらず活動できることが強みです。主に、貧困や紛争など、国際的な問題に取り組んでいます。
              </p>
            </div>
            <div className="AboutUs__Block5Left">
              <p className="AboutUs__Block5Title">NPO（非営利団体）</p>
              <p className="AboutUs__Block5Description">
                利益を目的とせず活動をする民間の団体です。ビジネスだけでは解決できない社会問題に取り組めることが特徴です。主に、国内の、教育支援や地域活性化等の活動に取り組んでいます。
              </p>
            </div>
          </div>
          <p className="AboutUs__Block5BottomText">
            ※ただし、NGO・NPOという名称として区別されていますが、実際には国際的に非営利で活動する団体も多くあります。
          </p>
        </section>
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(Aboutus);
