import React from 'react';
import ContainerTemplate from '../../../containers/ContainerTemplate';

import './_style.scss';

class PageTemplate extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
    }
    render() {
        return (
            <div>
              <h1>This is page component!</h1>
              <ContainerTemplate/>
            </div>
        );
    }
    componentDidMount() {
    }
    componentWillUnmount() {
    }
    componentWillReceiveProps(nextProps) {
    }
    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    componentWillUpdate(nextProps, nextState) {
    }
    componentDidUpdate(prevProps, prevState) {
    }
};

export default PageTemplate;
