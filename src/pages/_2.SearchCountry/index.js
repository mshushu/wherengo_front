import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';

import Header from '../../containers/_Header';
import StepBar from '../../containers/_StepBar';
import SelectPanel from '../../containers/_SelectPanel';
import SelectorMap from '../../containers/_SelectorMap';

import './_style.scss';

class SearchCountry extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {};
  render = () => {
    const {
      WorldArea,
      JapanArea,
      selectedCountry,
      selectedCategory,
      changeSelectedCountry,
      map,
      tapMap,
      setsSlectedAllJapanIsChecked,
      selectedAllJapanIsChecked,
      changeAllSelectedJapan,
      setsSlectedAllWorldIsChecked,
      selectedAllWorldIsChecked,
      changeAllSelectedWorld,
    } = this.props;

    localStorage.removeItem('selectedCountry');
    localStorage.removeItem('selectedCategory');
    localStorage.removeItem('selectedAllCategoryIsChecked');

    return (
      <div className="main mainFrame mainFrameSearch">
        <SelectPanel
          selectedCountry={selectedCountry}
          selectedCategory={selectedCategory}
          map={map}
          changeSelectedCountry={changeSelectedCountry}
          title="選択された場所"
          nextButtonTitle="カテゴリー選択画面へ"
          link="/Search/Category"
        />
        <StepBar />
        <SelectorMap
          WorldArea={WorldArea}
          JapanArea={JapanArea}
          selectedCountry={selectedCountry}
          map={map}
          tapMap={tapMap}
          changeSelectedCountry={changeSelectedCountry}
          setsSlectedAllJapanIsChecked={setsSlectedAllJapanIsChecked}
          selectedAllJapanIsChecked={selectedAllJapanIsChecked}
          changeAllSelectedJapan={changeAllSelectedJapan}
          setsSlectedAllWorldIsChecked={setsSlectedAllWorldIsChecked}
          selectedAllWorldIsChecked={selectedAllWorldIsChecked}
          changeAllSelectedWorld={changeAllSelectedWorld}
        />
      </div>
    );
  };
  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

// export default AdminLoginPageComponent;
export default connect()(SearchCountry);
