import React from 'react';
import { Route, Switch, Redirect } from 'react-router';

import { connect } from 'react-redux';

import Header from '../../containers/_Header';
import SearchCountry from '../..//pages/_2.SearchCountry';
import SearchCategory from '../../pages/_3.SearchCategory';
import SearchResult from '../../pages/_4.SearchResult';

import { getJapanArea } from '../../models/JapanArea/action';
import { getWorldArea } from '../../models/WorldArea/action';
import { getActivityType } from '../../models/ActivityType/action';

import './_style.scss';

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      map: 'world',
      selectedCountry: {
        jp: [],
        world: [],
      },
      selectedAllCategoryIsChecked: false,
      selectedCategory: [],
      selectedAllJapanIsChecked: false,
      selectedAllWorldIsChecked: false,
    };
  }
  componentWillMount = () => {
    this.props.dispatch(getJapanArea());
    this.props.dispatch(getWorldArea());
    this.props.dispatch(getActivityType());
  };
  render = () => {
    const parentURL = this.props.router.props.match.url;
    const { LANG, WorldArea, JapanArea, ActivityType } = this.props;
    const {
      selectedCountry,
      selectedCategory,
      map,
      tapMap,
      selectedAllCategoryIsChecked,
      selectedAllJapanIsChecked,
      selectedAllWorldIsChecked,
    } = this.state;

    return (
      <div className="main">
        <Header />
        <Switch>
          <Route
            path={`${parentURL}/Country`}
            component={(props, state, params) => (
              <SearchCountry
                LANG={LANG}
                WorldArea={WorldArea}
                JapanArea={JapanArea}
                selectedCountry={selectedCountry}
                selectedCategory={selectedCategory}
                changeSelectedCountry={this._changeSelectedCountry}
                map={map}
                tapMap={this._tapMap}
                setsSlectedAllJapanIsChecked={
                  this._setsSlectedAllJapanIsChecked
                }
                selectedAllJapanIsChecked={selectedAllJapanIsChecked}
                changeAllSelectedJapan={this._changeAllSelectedJapan}
                setsSlectedAllWorldIsChecked={
                  this._setsSlectedAllWorldIsChecked
                }
                selectedAllWorldIsChecked={selectedAllWorldIsChecked}
                changeAllSelectedWorld={this._changeAllSelectedWorld}
                router={{ props, state, params }}
              />
            )}
          />
          <Route
            path={`${parentURL}/Category`}
            component={(props, state, params) => (
              <SearchCategory
                LANG={LANG}
                router={{ props, state, params }}
                selectedCountry={selectedCountry}
                selectedCategory={selectedCategory}
                ActivityType={ActivityType}
                setSelectedCountry={this._setSelectedCountry}
                setSelectedCategory={this._setSelectedCategory}
                changeSelectedCategory={this._changeSelectedCategory}
                setsSlectedAllCategoryIsChecked={
                  this._setsSlectedAllCategoryIsChecked
                }
                selectedAllCategoryIsChecked={selectedAllCategoryIsChecked}
                changeAllSelectedCategory={this._changeAllSelectedCategory}
              />
            )}
          />
          <Route
            path={`${parentURL}/Result`}
            component={(props, state, params) => (
              <SearchResult
                LANG={LANG}
                router={{ props, state, params }}
                selectedCountry={selectedCountry}
                selectedCategory={selectedCategory}
              />
            )}
          />
        </Switch>
      </div>
    );
  };

  _tapMap = map => {
    this.setState({
      map: map,
    });
  };

  _changeSelectedCountry = e => {
    const { seq, name, map } = e.target.dataset;
    const item = {
      seq: seq,
      name: name,
    };
    let index = -1;

    for (let i = 0; i < this.state.selectedCountry[map].length; i++) {
      const { seq, name } = this.state.selectedCountry[map][i];
      if (item.seq == seq && item.name == name) {
        index = i;
      }
    }

    const title =
      'selectedAll' + (map == 'jp' ? 'Japan' : 'World') + 'IsChecked';

    if (e.target.checked) {
      if (index == -1) {
        this.setState(prevState => ({
          [title]:
            this.state.selectedCountry[map].length + 1 ==
            (map == 'jp'
              ? this.props.JapanArea.result.data.length
              : this.props.WorldArea.result.data.length),
          selectedCountry: {
            ...prevState.selectedCountry,
            [map]: [...this.state.selectedCountry[map], item],
          },
        }));
      }
    } else if (index >= 0) {
      this.setState(prevState => ({
        [title]: false,
        selectedCountry: {
          ...prevState.selectedCountry,
          [map]: this.state.selectedCountry[map].filter(
            (item, i) => i !== index
          ),
        },
      }));
    }
  };

  _setSelectedCountry = selectedCountry => {
    this.setState({
      selectedCountry,
    });
  };
  _setSelectedCategory = selectedCategory => {
    this.setState({
      selectedCategory,
    });
  };
  _setsSlectedAllCategoryIsChecked = selectedAllCategoryIsChecked => {
    this.setState({
      selectedAllCategoryIsChecked,
    });
  };

  _setsSlectedAllJapanIsChecked = selectedAllJapanIsChecked => {
    this.setState({
      selectedAllJapanIsChecked,
    });
  };
  _setsSlectedAllWorldIsChecked = selectedAllWorldIsChecked => {
    this.setState({
      selectedAllWorldIsChecked,
    });
  };
  _changeSelectedCategory = e => {
    const { seq, name, icon } = e.target.dataset;
    const item = {
      seq: seq,
      name: name,
      icon: icon,
    };
    let index = -1;

    for (let i = 0; i < this.state.selectedCategory.length; i++) {
      const { seq, name } = this.state.selectedCategory[i];
      if (item.seq == seq && item.name == name) {
        index = i;
      }
    }

    if (e.target.checked) {
      if (index == -1) {
        this.setState({
          selectedAllCategoryIsChecked:
            this.state.selectedCategory.length + 1 ==
            this.props.ActivityType.result.data.length,
          selectedCategory: [...this.state.selectedCategory, item],
        });
      }
    } else if (index >= 0) {
      this.setState({
        selectedAllCategoryIsChecked: false,
        selectedCategory: this.state.selectedCategory.filter(
          (item, i) => i !== index
        ),
      });
    }
  };

  _changeAllSelectedCategory = e => {
    const list = JSON.parse(e.target.dataset.list);

    if (e.target.checked) {
      this.setState({
        selectedAllCategoryIsChecked: true,
        selectedCategory: list,
      });
    } else {
      this.setState({
        selectedAllCategoryIsChecked: false,
        selectedCategory: [],
      });
    }
  };

  _changeAllSelectedJapan = e => {
    const list = JSON.parse(e.target.dataset.list);

    if (e.target.checked) {
      this.setState(prevState => ({
        selectedAllJapanIsChecked: true,
        selectedCountry: {
          ...prevState.selectedCountry,
          jp: list,
        },
      }));
    } else {
      this.setState(prevState => ({
        selectedAllJapanIsChecked: false,
        selectedCountry: {
          ...prevState.selectedCountry,
          jp: [],
        },
      }));
    }
  };

  _changeAllSelectedWorld = e => {
    const list = JSON.parse(e.target.dataset.list);

    if (e.target.checked) {
      this.setState(prevState => ({
        selectedAllWorldIsChecked: true,
        selectedCountry: {
          ...prevState.selectedCountry,
          world: list,
        },
      }));
    } else {
      this.setState(prevState => ({
        selectedAllWorldIsChecked: false,
        selectedCountry: {
          ...prevState.selectedCountry,
          world: [],
        },
      }));
    }
  };

  componentDidMount = () => {};
  componentWillUnmount = () => {};
  componentWillReceiveProps = nextProps => {};
  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };
  componentWillUpdate = (nextProps, nextState) => {};
  componentDidUpdate = (prevProps, prevState) => {};
}

function select(state) {
  return {
    WorldArea: state.WorldArea,
    JapanArea: state.JapanArea,
    ActivityType: state.ActivityType,
  };
}

// export default AdminLoginPageComponent;
export default connect(select)(Search);
