// For file/folder path
var path = require('path');
var webpack = require('webpack');
// 브라우저별 prefix 넣기: -ms-, -mz-등
var autoprefixer = require('autoprefixer');
// HTML 컴파일러, template 파일도 컴파일 가능
var HtmlWebpackPlugin = require('html-webpack-plugin');
// 컴파일이 완료되면 브라우저를 자동으로 연다.
var WebpackBrowserPlugin = require('webpack-browser-plugin');

module.exports = {
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/compile.js',
  ],

  output: {
    filename: 'js/app.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
  },
  // 디버깅을 위한 소스맵
  devtool: 'inline-source-map',

  module: {
    rules: [
      {
        // Reactjs 컴파일
        test: /\.(jsx|js)?$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        // scss 컴파일
        test: /\.scss$/,
        loaders: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
        exclude: /node_modules/,
      },
      {
        // 10k이하면 파일안에 인코딩하여 넣고, 10k보다 크면 파일을 복사한다.
        test: /\.(jpg|jpeg|gif|png|svg|ico)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'images/[name].[ext]',
              fallback: 'file-loader',
            },
          },
        ],
      },
      {
        // 10k이하면 파일안에 인코딩하여 넣고, 10k보다 크면 파일을 복사한다.
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'fonts/[name].[ext]',
              fallback: 'file-loader',
            },
          },
        ],
      },
    ],
  },

  plugins: [
    // HTML 컴파일
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: './index.html',
    }),
    // CSS Prefix 넣기
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [
          autoprefixer({ browsers: ['last 2 versions', '> 10%', 'ie 9'] }),
        ],
      },
    }),
    // Hot loading
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    // 브라우저 열기
    new WebpackBrowserPlugin(),
  ],
  devServer: {
    host: 'localhost',
    port: 3000,
    historyApiFallback: true,
    hot: true,
  },
};
