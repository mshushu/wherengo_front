// For file/folder path
var path = require('path');
var webpack = require('webpack');
// 브라우저별 prefix 넣기: -ms-, -mz-등
var autoprefixer = require('autoprefixer');
// HTML 컴파일러, template 파일도 컴파일 가능
var HtmlWebpackPlugin = require('html-webpack-plugin');
// CSS파일을 따로 저장하기 위한 플러그인
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: './src/compile.js',
  output: {
    filename: 'js/app.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
  },
  // 디버깅을 위한 플러그인, production 환경에서 제거 필요
  devtool: process.env.NODE_ENV !== 'production' ? 'source-map' : false,

  module: {
    rules: [
      {
        // Reactjs 컴파일
        test: /\.(jsx|js)?$/,
        use: [
          'babel-loader',
          'webpack-strip?strip[]=debug,strip[]=console.log,strip[]=console.dir',
        ],
        exclude: /node_modules/,
      },
      {
        // SCSS 컴파일
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              query: {
                importLoaders: 2,
                sourceMap: process.env.NODE_ENV !== 'production',
                minimize: process.env.NODE_ENV === 'production',
              },
            },
            {
              loader: 'postcss-loader',
              query: {
                sourceMap: process.env.NODE_ENV !== 'production',
              },
            },
            {
              loader: 'sass-loader',
              query: {
                sourceMap: process.env.NODE_ENV !== 'production',
              },
            },
          ],
          publicPath: '../',
        }),
      },
      {
        // 10k이하면 파일안에 인코딩하여 넣고, 10k보다 크면 파일을 복사한다.
        test: /\.(jpg|jpeg|gif|png|svg|ico)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'images/[name].[ext]',
              fallback: 'file-loader',
            },
          },
        ],
      },
      {
        // 10k이하면 파일안에 인코딩하여 넣고, 10k보다 크면 파일을 복사한다.
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'fonts/[name].[ext]',
              fallback: 'file-loader',
            },
          },
        ],
      },
    ],
  },

  plugins: [
    // JS 난독화
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: process.env.NODE_ENV !== 'production',
      comments: process.env.NODE_ENV !== 'production',
    }),
    // CSS Prefix 넣기
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [
          autoprefixer({ browsers: ['last 3 versions', '> 1%', 'ie 9'] }),
        ],
      },
    }),
    // HTML 컴파일, 난독화
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: './index.html',
      minify: {
        collapseWhitespace: process.env.NODE_ENV === 'production',
        keepClosingSlash: process.env.NODE_ENV === 'production',
        removeComments: process.env.NODE_ENV === 'production',
      },
      xhtml: process.env.NODE_ENV === 'production',
    }),
    // CSS 분리
    new ExtractTextPlugin({
      filename: 'css/style.css',
      disable: false,
      allChunks: true,
    }),
  ],
};
